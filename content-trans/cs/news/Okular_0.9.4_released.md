---
date: 2009-12-01
title: Vydán Okular 0.9.4
---
The forth maintenance release of the KDE 4.3 series includes Okular 0.9.4. It includes some crash fixes, and few small bug fixes in the interface. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
