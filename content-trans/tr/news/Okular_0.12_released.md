---
date: 2011-01-26
title: Okular 0.12 yayımlandı
---
Okular'ın 0.12 sürümü KDE uygulamalarının 4.6 sürümüyle birlikte yayımlandı. Bu yayım küçük hata düzültmeleriyle özellikler içeriyor ve Okular kullanan herkese güncelleme yapmaları önerilir.
