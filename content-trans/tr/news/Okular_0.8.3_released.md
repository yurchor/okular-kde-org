---
date: 2009-05-06
title: Okular 0.8.3 yayımlandı
---
KDE 4.2 serisinin üçüncü bakım sürümü Okular 0.8.3'ü içeriyor. Bu sürümde Okular için pek fazla haber yok, konuyla ilgili tek değişiklik XPS belgelerinin sayfa görüntüleri oluşturulurken daha güvenli iş parçacıklarının kullanımıyla ilgili. Tüm düzeltilmiş durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a> adresinde okuyabilirsiniz.
