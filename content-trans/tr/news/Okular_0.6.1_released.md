---
date: 2008-02-05
title: Okular 0.6.1 yayımlandı
---
KDE 4.0 serisinin ilk bakım sürümü Okular 0.6.1'i içeriyor. Bu sürüm tekrar indirilmeyen dosyaların daha iyi kaydedilmesi, kullanılabilirlik iyileştirmeleri dahil olmak üzere pek çok hata düzeltmesi içeriyor. Tüm düzeltilen durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a> adresinde okuyabilirsiniz.
