---
date: 2008-03-05
title: Okular 0.6.2 yayımlandı
---
KDE 4.0 serisinin ikinci bakım sürümü Okular 0.6.2'yi içeriyor. Bu sürüm belge kapatırken daha fazla kararlılık sağlanması ve yer imleme sisteminde küçük düzeltmeler dahil olmak üzere pek çok hata düzeltmesi içermektedir. Tüm düzeltilen durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a> adresinde okuyabilirsiniz.
