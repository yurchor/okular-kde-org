---
date: 2009-12-01
title: Okular 0.9.4 yayımlandı
---
KDE 4.3 serisinin dördüncü bakım sürümü Okular 0.9.4'ü içeriyor. Bu sürümde bazı çöküş düzeltmeleri ve arayüzde bazı küçük hata düzeltmeleri bulunuyor. Tüm düzeltilen durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a> adresinde okuyabilirsiniz.
