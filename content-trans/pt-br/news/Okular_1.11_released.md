---
date: 2020-08-13
title: O Okular 1.11 foi lançado
---
A versão 1.11 do Okular foi lançada. Esta versão introduz uma interface nova de anotação e várias correções menores e melhorias por todo lugar. Você pode verificar o registro de alterações completo em <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. O Okular 1.11 é uma atualização recomendada a todos os usuários do Okular.
