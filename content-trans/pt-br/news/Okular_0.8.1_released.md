---
date: 2009-03-04
title: O Okular 0.8.1 foi lançado
---
A primeira versão de manutenção da série KDE 4.2 inclui o Okular 0.8.1. Ela incorpora algumas correções de falhas nas infraestruturas CHM e DjVu e pequenas correções na interface do usuário. Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
