---
date: 2010-08-10
title: O Okular 0.11 foi lançado
---
A versão 0.11 do Okular foi lançada junto com a versão 4.5 do KDE Applications. Esta versão introduz algumas pequenas correções e funcionalidades, e é uma atualização recomendada a todos os usuários do Okular.
