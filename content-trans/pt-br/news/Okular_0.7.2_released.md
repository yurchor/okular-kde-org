---
date: 2008-10-03
title: O Okular 0.7.2 foi lançado
---
A segunda versão de manutenção da série KDE 4.1 inclui o Okular 0.7.2. Ela incorpora algumas pequenas correções nas infraestruturas TIFF e Comicbook e altera o nível de zoom padrão para "Ajustar à largura". Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
