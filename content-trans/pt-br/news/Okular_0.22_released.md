---
date: 2015-04-15
title: O Okular 0.22 foi lançado
---
A versão 0.22 do Okular foi lançada junto com a versão 15.04 do KDE Applications. Esta versão do Okular é uma atualização recomendada a todos os usuários do programa.
