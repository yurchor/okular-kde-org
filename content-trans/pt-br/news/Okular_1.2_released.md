---
date: 2017-08-17
title: O Okular 1.2 foi lançado
---
A versão 1.2 do Okular foi lançada junto com o KDE Applications versão 17.08. Esta versão introduz pequenas correções e melhorias. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. O Okular 1.2 é uma atualização recomendada a todos os usuários do Okular.
