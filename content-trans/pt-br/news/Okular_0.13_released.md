---
date: 2011-07-27
title: O Okular 0.13 foi lançado
---
A versão 0.13 do Okular foi lançada junto com a versão 4.7 do KDE Applications. Esta versão introduz algumas pequenas correções e funcionalidades, e é uma atualização recomendada a todos os usuários do Okular.
