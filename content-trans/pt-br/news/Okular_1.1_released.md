---
date: 2017-04-20
title: O Okular 1.1 foi lançado
---
A versão 1.1 do Okular foi lançada junto com o KDE Applications versão 17.04. Esta versão introduz a funcionalidade de redimensionar anotações, suporte a cálculo automático de conteúdos de formulários, melhorias no touch screen e mais! Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. O Okular 1.1 é uma atualização recomendada a todos os usuários do Okular.
