---
date: 2013-12-12
title: Okular 0.18 izdan
---
Verzija 0.18 programa Okular je objavljena zajedno s KDE Applications 4.12 izdanjem. ovo izdanje uključuje nove mogućnosti kao što je audio/video podrška u EPub datotekama i poboljšanja postojećih mogućnosti poput traženja i štampanja. Okular 0.18 je preporučena nadogradnja za svakoga ko koristi Okular.
