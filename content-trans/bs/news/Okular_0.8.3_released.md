---
date: 2009-05-06
title: Okular 0.8.3 pušten
---
Treće izdanje KDE 4.2 serije uključuje Okular 0.8.3. Ono ne pruža mnogo vijesti za Okular, jedina relevantna promjena je pružanje veće sigurnosti prilikom stvaranja stranice slike u XPS dokumentu. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
