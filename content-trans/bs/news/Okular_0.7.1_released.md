---
date: 2008-09-03
title: Okular 0.7.1 pušten
---
Prvo izdanje KDE 4.1 serije uključuje Okular 0.7.1. Ono između ostalog uključuje neke popravke krahiranja. Sve riješene probleme vezane za ovu temu možete pronaći na <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
