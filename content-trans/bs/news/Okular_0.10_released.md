---
date: 2010-02-09
title: Okular 0.10 pušten
---
Verzija 0.10 Okulara je puštena zajedno sa KDE SC 4.4 aplikacijom. Osim osnovnih poboljšanja u stabilnosti ovo izdanje podržava novu / poboljšanu podršku za obje vrste pretrage, naprijed i obrnutu te povezuje retke izvornog koda s odgovarajućim mjestima u dvi i pdf datotekama.
