---
date: 2009-04-02
title: Okular 0.8.2 pušten
---
Drugo izdanje KDE 4.2 serije uključuje Okular 0.8.2. Uključuje bolju podršku (nadamo se da radi) za DVI i pdfsync obrnutu pretragu, i popravke i mala poboljšanja u načinu prezentacije. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
