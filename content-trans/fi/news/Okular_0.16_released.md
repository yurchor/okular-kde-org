---
date: 2013-02-06
title: Okular 0.16 julkaistiin
---
Okularin versio 0.16 on julkaistu KDE:n sovellusten 4.10-julkaisuversiossa. Tämä julkaisuversio esittelee uusia ominaisuuksia kuten mahdollisuuden zoomata PDF-asiakirjoja entistä lähemmäksi, Activeen pohjautuvan katselimen tableteille, lisää tukea PDF-elokuville, valintaa seuraavan näkymän sekä parannuksia merkintöjen muokkaamiseen. Okular 0.16 on suositeltu päivitys kaikille Okularin käyttäjille.
