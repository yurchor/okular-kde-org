---
date: 2011-07-27
title: Okular 0.13 julkaistiin
---
Okularin versio 0.13 on julkaistu KDE:n sovellusten 4.7-julkaisuversiossa. Tämä julkaisuversio esittelee pieniä korjauksia sekä ominaisuuksia, ja on suositeltu päivitys kaikille Okularin käyttäjille.
