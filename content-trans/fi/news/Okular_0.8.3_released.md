---
date: 2009-05-06
title: Okular 0.8.3 julkaistiin
---
Okular 0.8.3 on julkaistu KDE 4.2-sarjan kolmannessa korjausjulkaisussa. Tässä julkaisuversiossa ei ole paljon uutta. Ainut olennainen muutos koskee suurempaa säieturvallisuutta luotaessa XPS-asiakirjojen sivujen kuvia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
