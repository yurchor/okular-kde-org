---
date: 2013-12-12
title: Okular 0.18 julkaistiin
---
Okularin versio 0.18 on julkaistu KDE:n sovellusten 4.12-julkaisuversiossa. Tämä julkaisuversio esittelee uusia ominaisuuksia kuten äänen ja videon tuen EPub-tiedostoille sekä parannuksia olemassa oleviin ominaisuuksiin kuten hakuun ja tulostukseen. Okular 0.18 on suositeltu päivitys kaikille Okularin käyttäjille.
