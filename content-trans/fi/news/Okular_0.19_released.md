---
date: 2014-04-16
title: Okular 0.19 julkaistiin
---
Okularin versio 0.19 on julkaistu KDE:n sovellusten 4.13-julkaisuversiossa. Tämä julkaisuversio esittelee uusia ominaisuuksia kuten välilehdet, näytön DPI:n käytön, jotta sivun koko vastaa todellista paperikokoa, parannuksia toimintojen kumoamiseen ja uudelleen tekemiseen sekä muita ominaisuuksia/parannuksia. Okular 0.19 on suositeltu päivitys kaikille Okularin käyttäjille.
