---
date: 2008-02-05
title: Okular 0.6.1 julkaistiin
---
Okular 0.6.1 on julkaistu KDE 4.0-sarjan ensimmäisessä korjausjulkaisussa. Tähän julkaisuversioon kuuluu aika monta virhekorjausta kuten tiedostojen uudelleenlataamisen välttäminen niitä tallennettaessa, käytettävyysparannuksia jne. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
