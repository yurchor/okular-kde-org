---
date: 2009-12-01
title: Okular 0.9.4 julkaistiin
---
Okular 0.9.4 on julkaistu KDE 4.3-sarjan neljännessä korjausjulkaisussa. Tähän julkaisuversioon kuuluu joitakin kaatumiskorjauksia sekä pieniä käyttöliittymän virhekorjauksia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
