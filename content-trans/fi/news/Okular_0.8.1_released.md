---
date: 2009-03-04
title: Okular 0.8.1 julkaistiin
---
Okular 0.8.1 on julkaistu KDE 4.2-sarjan ensimmäisessä korjausjulkaisussa. Tähän julkaisuversioon kuuluu joitakin kaatumiskorjauksia CHM- ja DjVu-taustaosissa sekä pieniä käyttöliittymäkorjauksia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
