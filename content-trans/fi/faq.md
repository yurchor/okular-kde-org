---
faq:
- answer: Ubuntu (thus Kubuntu as well) packages of Okular are compiled without support
    for these two formats. The reason is explained in [this](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad report.
  question: When using Ubuntu, I cannot read CHM and EPub documents, even if I have
    okular-extra-backends and libchm1 installed. Why?
- answer: Because you don't have a speech service on your system. Install the Qt Speech
    library and they should be enabled
  question: Why are the speak options in the Tools menu grayed out?
- answer: Asenna poppler-data -paketti
  question: Joitakin merkkejä ei piirretty, ja kun virheenpaikannus otettiin käyttöön,
    joillakin riveillä mainitaan ”kieleltä xxx puuttuva kielipaketti”
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Usein kysytyt kysymykset
---
