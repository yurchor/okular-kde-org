---
date: 2008-02-05
title: Publicouse Okular 0.6.1
---
A primeira versión de mantemento da serie 4.0 de KDE inclúe Okular 0.6.1. Inclúe bastantes correccións de fallos, entre elas melloras no sistema que evita repetir descargas ao gardar, melloras de experiencia de usuario, etc. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
