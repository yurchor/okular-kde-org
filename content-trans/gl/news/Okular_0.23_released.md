---
date: 2015-08-19
title: Publicouse Okular 0.23
---
A versión 0.23 de Okular publicouse coa versión 15.08 das aplicacións de KDE. Esta versión permite usar a transición de esvaer no modo de presentación, así como algunhas solucións de problemas sobre anotacións e reprodución de vídeo. Okular 0.23 é unha actualización recomendada para calquera que use Okular.
