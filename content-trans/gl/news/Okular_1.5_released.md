---
date: 2018-08-16
title: Publicouse Okular 1.5
---
A versión 1.5 de Okular publicouse coa versión 18.08 das aplicacións de KDE. Esta versión introduce melloras nos formularios e moitos outras solucións de erros e pequenas funcionalidades. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 é unha actualización recomendada para calquera que use Okular.
