---
date: 2011-07-27
title: Publicouse Okular 0.13
---
A versión 0.13 de Okular publicouse xunto coa versión 4.7 das aplicacións de KDE. Esta versión introduce pequenas correccións e funcionalidades e recoméndase para calquera que use Okular.
