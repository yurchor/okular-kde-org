---
date: 2016-12-15
title: Publicouse Okular 1.0
---
A versión 1.0 de Okular publicouse coa versión 16.12 das aplicacións de KDE. Esta versión está baseada na versión 5 das infraestruturas de KDE, pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 é unha actualización recomendada para calquera que use Okular.
