---
date: 2015-04-15
title: Publicouse Okular 0.22
---
A versión 0.22 de Okular publicouse coa versión 15.04 das aplicacións de KDE. Okular 0.22 é unha actualización recomendada para calquera que use Okular.
