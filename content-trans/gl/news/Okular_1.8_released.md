---
date: 2019-08-15
title: Publicouse Okular 1.8
---
A versión 1.8 de Okular publicouse coa versión 19.08 das aplicacións de KDE. Esta versión introduce a funcionalidade de definir os finais das anotacións de liña e moitos outras solucións de erros e pequenas funcionalidades. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=19.08.0#okular</a>. Okular 1.8 é unha actualización recomendada para calquera que use Okular.
