---
date: 2009-12-01
title: Publicouse Okular 0.9.4
---
A cuarta versión de mantemento da serie KDE 4.3 inclúe Okular 0.9.4. Inclúe correccións de quebras e algunhas pequenas correccións de fallos na interface. Pode ler todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
