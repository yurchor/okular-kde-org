---
date: 2009-06-03
title: Publicouse Okular 0.8.4
---
A cuarta versión de mantemento da serie KDE 4.2 inclúe Okular 0.8.4. Inclúe correccións en documentos de texto de OpenDocument, un par de correccións de quebras e algunhas pequenas correccións de fallos na interface. Pode ler todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
