---
date: 2008-11-04
title: Publicouse Okular 0.7.3
---
A terceira versión de mantemento da serie 4.1 de KDE inclúe Okular 0.7.3. Inclúe pequenas correccións na interface de usuario e na busca de texto. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
