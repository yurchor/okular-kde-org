---
menu:
  main:
    parent: about
    weight: 4
title: ئالاقەداش
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

You can contact the Okular team in many ways:

* Mailing list: To coordinate okular development, we use the [okular-devel mailing list](https://mail.kde.org/mailman/listinfo/okular-devel) at kde.org. You can use it to talk about the development of the core application, and feedback about existent or new backends is appreciated.

* IRC: For general chat, we use the IRC [#okular](irc://irc.kde.org/#okular) and [#kde-devel](irc://irc.kde.org/#kde-devel) on the [Freenode network](http://www.freenode.net/). Some of the Okular developers can be found hanging there.

* Matrix: The aforementioned chat can also be accessed over the Matrix network via [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum: If you prefer to use a forum you can use the [Okular forum](http://forum.kde.org/viewforum.php?f=251) inside the bigger [KDE Community Forums](http://forum.kde.org/).

* Bugs and Wishes: Bugs and Wishes should be reported to the [KDE bug tracker](https://bugs.kde.org/). If you want to help, you can find a list of top bugs [here](https://community.kde.org/Okular).
