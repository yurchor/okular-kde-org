---
date: 2017-04-20
title: Okular 1.1 released
---
The 1.1 version of Okular has been released together with KDE Applications 17.04 release. This release introduces annotation resize functionality, support for automatic calculation of form contents, touch screen improvements and more! You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 is a recommended update for everyone using Okular.
