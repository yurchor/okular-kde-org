---
date: 2011-07-27
title: صدر أوكلار 0.13
---
The 0.13 version of Okular has been released together with KDE Applications 4.7 release. This release introduce small fixes and features and is a recommended update for everyone using Okular.
