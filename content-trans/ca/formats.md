---
intro: L'Okular accepta una gran varietat de formats de documents i casos d'ús. Aquesta
  pàgina sempre fa referència a les sèries estables de l'Okular, actualment l'Okular
  20.12
layout: formats
menu:
  main:
    name: Formats de document
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Estat dels gestors de format de document
---
