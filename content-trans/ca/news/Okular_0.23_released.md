---
date: 2015-08-19
title: Publicat l'Okular 0.23
---
S'ha publicat la versió 0.23 de l'Okular conjuntament amb la publicació 15.08 de les Aplicacions del KDE. Aquesta publicació presenta la implementació de la transició d'esvaïment en el mode presentació, així com esmenes d'errors relacionades amb les anotacions i la reproducció de vídeo. L'Okular 0.23 és una actualització recomanada per a tothom que usi l'Okular.
