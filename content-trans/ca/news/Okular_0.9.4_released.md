---
date: 2009-12-01
title: Publicat l'Okular 0.9.4
---
La quarta publicació de manteniment de les sèries 4.3 del KDE conté l'Okular 0.9.4. Inclou diverses esmenes de fallades, així com esmenes d'errors a la interfície. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
