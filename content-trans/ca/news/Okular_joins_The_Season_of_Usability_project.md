---
date: 2007-01-31
title: L'Okular s'uneix al projecte «Season of Usability»
---
L'equip de l'Okular anuncia amb satisfacció que l'Okular ha estat una de les aplicacions seleccionades per a participar en el projecte <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, organitzat per experts en usabilitat a <a href="http://www.openusability.org">OpenUsability</a>. Des d'aquí volem donar la benvinguda al Sharad Baliyan a l'equip i agrair al Florian Graessle i al Pino Toscano per la seva tasca continuada en millorar l'Okular.
