---
date: 2011-01-26
title: Publicat l'Okular 0.12
---
S'ha publicat la versió 0.12 de l'Okular conjuntament amb la publicació 4.6 de les Aplicacions del KDE. Aquesta publicació presenta petites esmenes d'errors i característiques i és una actualització recomanada per a tothom que usi l'Okular.
