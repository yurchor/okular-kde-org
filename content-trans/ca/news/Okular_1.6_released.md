---
date: 2018-12-13
title: Publicat l'Okular 1.6
---
S'ha publicat la versió 1.6 de l'Okular conjuntament amb la publicació 18.12 de les Aplicacions del KDE. Aquesta publicació presenta l'eina d'anotacions nova de màquina d'escriure, entre altres esmenes i petites característiques. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0#okular</a>. L'Okular 1.6 és una actualització recomanada per a tothom que usi l'Okular.
