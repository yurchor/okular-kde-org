---
date: 2008-02-05
title: Publicat l'Okular 0.6.1
---
La primera publicació de manteniment de les sèries 4.0 del KDE conté l'Okular 0.6.1. Inclou diverses esmenes d'errors, incloent una millora per a no tronar a baixar fitxers en desar, millores d'usabilitat, etc. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
