---
date: 2008-11-04
title: Publicat l'Okular 0.7.3
---
La tercera publicació de manteniment de les sèries 4.1 del KDE conté l'Okular 0.7.3. Inclou diversos esmenes secundàries a la interfície d'usuari i a la cerca de text. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
