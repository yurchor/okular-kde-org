---
date: 2017-04-20
title: Publicat l'Okular 1.1
---
S'ha publicat la versió 1.1 de l'Okular conjuntament amb la publicació 17.04 de les Aplicacions del KDE. Aquesta publicació presenta la característica per a canviar de mida les anotacions, la implementació per a calcular automàticament el contingut dels formularis, millores per a les pantalles tàctils i més! Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. L'Okular 1.1 és una actualització recomanada per a tothom que usi l'Okular.
