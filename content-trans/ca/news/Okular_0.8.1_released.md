---
date: 2009-03-04
title: Publicat l'Okular 0.8.1
---
La primera publicació de manteniment de les sèries 4.2 del KDE conté l'Okular 0.8.1. Inclou diverses esmenes de fallades als dorsals CHM i DjVu, i petites esmenes a la interfície d'usuari. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
