---
date: 2018-04-19
title: Publicat l'Okular 1.4
---
S'ha publicat la versió 1.4 de l'Okular conjuntament amb la publicació 18.04 de les Aplicacions del KDE. Aquesta publicació presenta millores a la implementació de JavaScript en els fitxers PDF i permet la cancel·lació de la renderització de PDF, el qual vol dir que si teniu un fitxer PDF complex i canvieu el mentre s'està renderitzant, es cancel·larà immediatament en lloc d'esperar la finalització de la renderització. Podeu revisar el registre complet de canvis a <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. L'Okular 1.4 és una actualització recomanada per a tothom que usi l'Okular.
