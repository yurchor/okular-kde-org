---
date: 2008-10-03
title: Publicat l'Okular 0.7.2
---
La segona publicació de manteniment de les sèries 4.1 del KDE conté l'Okular 0.7.2. Inclou diversos esmenes secundàries als dorsals TIFF i Comicbook i el canvi a «Ajusta a l'amplada de pàgina»com a nivell de zoom predeterminat. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
