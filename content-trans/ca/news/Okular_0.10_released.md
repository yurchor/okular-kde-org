---
date: 2010-02-09
title: Publicat l'Okular 0.10
---
S'ha publicat la versió 0.10 de l'Okular conjuntament amb la publicació del KDE SC 4.4. A banda de les millores generals en estabilitat, aquest publicació aporta un suport nou/millorat per a la cerca inversa i directa enllaçant les línies del codi font del LaTeX amb les ubicacions corresponents als fitxers DVI i PDF.
