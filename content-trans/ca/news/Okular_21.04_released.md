---
date: 2021-04-22
title: Publicat l'Okular 21.04
---
S'ha publicat la versió 21.04 de l'Okular. Aquest llançament presenta la signatura digital de fitxers PDF i diverses correccions i característiques secundaries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. L'Okular 21.04 és una actualització recomanada per a tothom que usi l'Okular.
