---
date: 2008-03-05
title: Выпущен Okular 0.6.2
---
Второй выпуск KDE 4.0 включает в себя Okular 0.6.2. В Okular 0.6.2 было сделано довольно много исправлений, включая улучшение стабильности при закрытии документа, а также маленькие исправления в системе закладок. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
