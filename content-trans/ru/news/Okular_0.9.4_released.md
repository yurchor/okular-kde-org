---
date: 2009-12-01
title: Выпущен Okular 0.9.4
---
Четвёртый выпуск KDE 4.3 включает в себя Okular 0.9.4. В Okular 0.9.4 были исправлены ошибки, приводящие к аварийному завершению программы, а также некоторые небольшие исправления в интерфейсе. Вы можете прочитать больше обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
