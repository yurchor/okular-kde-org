---
date: 2008-11-04
title: Выпущен Okular 0.7.3
---
Третий выпуск KDE 4.1 включает в себя Okular 0.7.3. В Okular 0.7.3 были исправлены некоторые небольшие ошибки в пользовательском интерфейсе и поиске. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
