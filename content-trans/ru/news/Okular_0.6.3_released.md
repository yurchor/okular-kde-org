---
date: 2008-04-02
title: Выпущен Okular 0.6.3
---
Третий выпуск KDE 4.0 включает в себя Okular 0.6.3. В Okular 0.6.3 были исправлены некоторые ошибки, например лучший способ получить позицию текста в документах PDF, а также несколько исправлений для системы аннотаций, и поддержка оглавлений. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
