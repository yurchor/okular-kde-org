---
date: 2018-04-19
title: Выпущен Okular 1.4
---
The 1.4 version of Okular has been released together with KDE Applications 18.04 release. This release introduces improved JavaScript support in PDF files and supports PDF rendering cancelling, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Okular 1.4 is a recommended update for everyone using Okular.
