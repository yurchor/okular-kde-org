---
date: 2009-06-03
title: Выпущен Okular 0.8.4
---
Четвёртый выпуск KDE 4.2 включает в себя Okular 0.8.4. В Okular 0.8.4 были исправлены некоторые ошибки в текстовых документах OpenDocument, были исправлены ошибки, приводящие к аварийному завершению программы, а также в этот выпуск вошли небольшие исправления интерфейса. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
