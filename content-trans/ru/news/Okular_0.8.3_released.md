---
date: 2009-05-06
title: Выпущен Okular 0.8.3
---
Третий выпуск KDE 4.2 включает в себя Okular 0.8.3. В этом выпуске нет важных исправлений Okular, только изменения алгоритма генерации изображений страниц документа формата XPS. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
