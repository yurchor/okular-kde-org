---
date: 2019-08-15
title: Okular 1.8 출시
---
Okular의 1.8 버전은 KDE 프로그램 19.08과 함께 출시되었습니다. 이 릴리스에서는 새로운 타자기 주석 도구를 비롯한 다양한 기타 수정 사항과 여러 기능이 추가되었습니다. 전체 변경 내역은 <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a> 페이지에서 확인할 수 있습니다. Okular 1.8은 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
