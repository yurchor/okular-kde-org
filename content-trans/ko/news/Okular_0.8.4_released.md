---
date: 2009-06-03
title: Okular 0.8.4 출시
---
KDE 4.2 시리즈의 네 번째 유지 보수 릴리스에는 Okular 0.8.4가 들어 있습니다. 이 릴리스에는 OpenDocument 텍스트 문서 처리 개선, 여러 가지 충돌 개선 및 인터페이스 버그 수정이 들어 있습니다. 전체 변경 사항은 <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a> 페이지에서 확인할 수 있습니다
