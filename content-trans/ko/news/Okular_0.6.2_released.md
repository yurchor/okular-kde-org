---
date: 2008-03-05
title: Okular 0.6.2 출시
---
KDE 4.0 시리즈의 두 번째 유지 보수 릴리스에는 Okular 0.6.2가 들어 있습니다. 이 릴리스에는 문서를 닫을 때 안정성 개선, 책갈피 시스템 개선 등 여러 버그가 수정되었습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a> 페이지를 방문하십시오
