---
date: 2008-10-03
title: Okular 0.7.2 출시
---
KDE 4.1 시리즈의 두 번째 유지 보수 릴리스에는 Okular 0.7.2가 들어 있습니다. 이 릴리스에는 TIFF 및 Comicbook 백엔드의 사소한 기능 개선과 기본 크기 조정 단계를 "폭 맞춤"으로 변경했습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a> 페이지를 방문하십시오
