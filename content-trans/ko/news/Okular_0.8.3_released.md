---
date: 2009-05-06
title: Okular 0.8.3 출시
---
KDE 4.2 시리즈의 세 번째 유지 보수 릴리스에는 Okular 0.8.3이 들어 있습니다. Okular 변경 내역은 많지 않지만, 주요 변경 사항은 XPS 문서의 쪽 그림을 생성할 때 스레드 안정성 개선이 있습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a> 페이지를 방문하십시오
