---
date: 2009-12-01
title: Okular 0.9.4 출시
---
KDE 4.3 시리즈의 네 번째 유지 보수 릴리스에는 Okular 0.9.4가 들어 있습니다. 이 릴리스에는 충돌 오류 수정 및 인터페이스 버그 수정이 들어 있습니다. 전체 변경 내역을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a> 페이지를 방문하십시오
