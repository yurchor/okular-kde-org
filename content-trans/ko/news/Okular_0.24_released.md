---
date: 2015-12-16
title: Okular 0.24 출시
---
Okular 0.24 버전은 KDE 프로그램 15.12와 함께 출시되었습니다. 이 릴리스에는 사소한 버그 수정과 기능 개선이 있었습니다. 전체 변경 내역을 확인하려면 <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a> 페이지를 방문하십시오. Okular 0.24는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
