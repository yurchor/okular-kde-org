---
date: 2017-08-17
title: Okular 1.2 출시
---
Okular 1.2 버전은 KDE 프로그램 17.08 릴리스와 함께 출시되었습니다. 이 릴리스에서는 사소한 버그 수정 및 기능 개선이 있었습니다. 전체 변경 내역을 확인하려면 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a> 페이지를 방문하십시오. Okular 1.2는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
