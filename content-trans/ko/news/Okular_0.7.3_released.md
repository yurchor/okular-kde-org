---
date: 2008-11-04
title: Okular 0.7.3 출시
---
KDE 4.1 시리즈의 세 번째 유지 보수 릴리스에는 Okular 0.7.3이 들어 있습니다. 이 릴리스에는 사용자 인터페이스와 텍스트 검색의 사소한 오류 수정 및 개선이 들어 있습니다. 전체 변경 내역을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a> 페이지를 방문하십시오
