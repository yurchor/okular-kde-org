---
menu:
  main:
    name: Ingebouwd
    parent: about
    weight: 2
title: Okular compileren vanuit de broncode op Linux
---
<span style="background-color:#e8f4fa">Als u zoekt naar de voorgecompileerde pakketten, bezoek de [downloadpagina](/download/). U kunt de pakketstatus [hier](https://repology.org/project/okular/versions)</span> controleren.

Als u Okular wilt compileren moet u een compilatieomgeving opzetten, die, in het algemeen, geleverd zou moeten zijn door uw distributie. In het geval dat u de ontwikkelversie van Okular wilt compileren, kijk dan op <a href='%1'>Bouwen vanuit de broncode</a> op de wiki van de gemeenschap van KDE.

U kunt Okular op deze manier downloaden en compileren:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Als u Okular installeert in een ander pad dan de installatiemap van uw systeem, dan is het mogelijk dat u uitvoert `source build/prefix.sh; okular` zodat het juiste exemplaar van Okular en bibliotheken opgepikt worden.

## Optionele pakketten

Er zijn enige optionele pakketten die u zou kunnen installeren om wat meer functionaliteiten in Okular te hebben. Sommigen zouden al geleverd kunnen zijn in uw distributie, maar andere niet. Als u elk probleem wilt vermijden, blijf dan bij de pakketten ondersteunt door uw distributie

* Poppler (PDF-backend): Om de PDF-backend te compileren hebt u [de Poppler bibliotheek](http://poppler.freedesktop.org) nodig, waarvoor de minimaal vereiste versie 0.24 is.
* Libspectre: Om deze PostScipt (PS) backend te compileren en te gebruiken, hebt u libspectre >= 0.2 nodig. Als uw distributie libspectre niet mee levert of de versie van het pakket is niet hoog genoeg, dan kunt u het [hier](http://libspectre.freedesktop.org) downloaden.
* DjVuLibre: on de DjVu-backend te compileren hebt u DjVuLibre >= 3.5.17 nodig. Net als met Libspectre kunt u het uit uw distributie ophalen of [hier](http://djvulibre.djvuzone.org).
* libTIFF: dit is vereist voor TIFF/fax ondersteuning. Op dit moment is er geen minimaal vereiste versie, dus elke tamelijk recente versie van de bibliotheek, die beschikbaar is in uw distributie zou moeten werken. In geval van problemen daarmee, aarzel dan niet om contact op te nemen met de ontwikkelaars van Okular.
* libCHM: dit is nodig om de CHM-backend te compileren. Evenals met libTIFF is er geen minimum versie vereist
* Libepub: als u ondersteuning van EPub nodig hebt dan kunt u deze bibliotheek uit uw distributie installeren of uit [sourceforge](http://sourceforge.net/projects/ebook-tools).
