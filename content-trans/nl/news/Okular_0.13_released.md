---
date: 2011-07-27
title: Okular 0.13 vrijgegeven
---
De 0.13 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.7. Deze vrijgave introduceert kleine reparaties en biedt extra mogelijkheden en wordt aanbevolen aan iedereen die Okular gebruikt.
