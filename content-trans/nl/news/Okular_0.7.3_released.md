---
date: 2008-11-04
title: Okular 0.7.3 vrijgegeven
---
De derde onderhoudsuitgave van de KDE 4.1 serie bevat Okular 0.7.3. Het bevat reparaties in het interface en in het zoeken naar tekst. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
