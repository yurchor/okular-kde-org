---
date: 2014-12-17
title: Okular 0.21 vrijgegeven
---
De 0.21 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 14.12. Deze vrijgave introduceert nieuwe functies zoals ondersteuning van latex-synctex omgekeerd zoeken in dvi en kleine reparaties van bugs. Okular 0.21 wordt aanbevolen aan iedereen die Okular gebruikt.
