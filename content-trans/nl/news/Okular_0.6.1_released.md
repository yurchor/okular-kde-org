---
date: 2008-02-05
title: Okular 0.6.1 vrijgegeven
---
De eerste onderhoudsuitgave van de KDE 4.0 serie bevat Okular 0.6.1. Het bevat een behoorlijk aantal reparaties van bugs, waaronder een betere manier om bestanden niet opnieuw te doenloaden bij opslaan, verbeteringen bij het gebruik, etc. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
