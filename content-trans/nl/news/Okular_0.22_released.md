---
date: 2015-04-15
title: Okular 0.22 vrijgegeven
---
De 0.22 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 15.04. Okular 0.22 wordt aanbevolen aan iedereen die Okular gebruikt.
