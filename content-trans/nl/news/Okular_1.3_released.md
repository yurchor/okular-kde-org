---
date: 2017-12-14
title: Okular 1.3 vrijgegeven
---
De versie 1.3 van Okular is vrijgegeven samen met de vrijgave van KDE Applications 17.12. Deze vrijgave introduceert wijzigingen over hoe annotaties op te slaan en formuliergegevens, ondersteunt gedeeltelijke rendering van het bijwerken van bestanden die veel tijd nemen om te renderen, die koppelingen naar teksten interactief maken in tekstselectiemodus, een optie gedeeld in het menu Bestand toevoegt, ondersteuning voor Markdown toevoegt en enige problemen repareert met betrekking tot ondersteuning van HiDPI. U kunt de volledige wijzigingslog raadplegen op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 is een aanbevolen update voor iedereen die Okular gebruikt.
