---
date: 2008-10-03
title: Okular 0.7.2 vrijgegeven
---
De tweede onderhoudsuitgave van de KDE 4.1 serie bevat Okular 0.7.2. Het bevat kleine reparaties in de TIFF- en Comicbook-backends en de wijziging naar "Passend in breedte" als de standaard voor het zoomniveau. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
