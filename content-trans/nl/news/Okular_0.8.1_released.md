---
date: 2009-03-04
title: Okular 0.8.1 vrijgegeven
---
De eerste onderhoudsuitgave van de KDE 4.2 serie bevat Okular 0.8.1. Het bevat reparaties van enige crashes in de CHM- en DjVU-backends en een paar kleine reparaties in het gebruikersinterface. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
