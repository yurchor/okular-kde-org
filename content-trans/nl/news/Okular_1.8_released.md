---
date: 2019-08-15
title: Okular 1.8 vrijgegeven
---
De 1.8 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 19.08. Deze vrijgave introduceert de mogelijkheid annotaties aan het einde van regels in te stellen naast verschillende andere reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Okular 1.8 wordt aanbevolen aan iedereen die Okular gebruikt.
