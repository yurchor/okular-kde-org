---
date: 2017-04-20
title: Okular 1.1 vrijgegeven
---
De versie 1.1 van Okular is vrijgegeven samen met de vrijgave van KDE Applications 17.04. Deze vrijgave introduceert functionaliteit voor in grootte wijzigen van annotaties, ondersteuning voor automatisch berekenen van inhoud van formulieren, verbeteringen voor aanraakschermen en meer! U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 wordt aanbevolen aan iedereen die Okular gebruikt.
