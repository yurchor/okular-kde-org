---
date: 2012-10-10
title: Okular-forums
---
We hebben nu een subforum in de Forums van de KDE gemeenschap. U kunt deze vinden op <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
