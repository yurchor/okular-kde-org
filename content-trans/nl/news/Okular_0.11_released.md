---
date: 2010-08-10
title: Okular 0.11 vrijgegeven
---
De 0.11 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.5. Deze vrijgave introduceert kleine reparaties en biedt extra mogelijkheden en wordt aanbevolen aan iedereen die Okular gebruikt.
