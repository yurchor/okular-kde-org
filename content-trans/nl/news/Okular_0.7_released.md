---
date: 2008-07-29
title: Okular 0.7 vrijgegeven
---
Het team van Okular is trots om een nieuwe versie van Okular aan te kondigen, vrijgegeven als onderdeel van <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Enige van de nieuwe mogelijkheden en verbeteringen omvatten (in willekeurige volgorde):

* Mogelijkheid om een PDF-document op te slaan met de wijzigingen in de formuliervelden
* Presentatiemodus: ondersteuning voor meer dan één scherm en mogelijkheid om transities uit te schakelen
* Het zoomniveau wordt nu per-document opgeslagen
* Verbeterde ondersteuning voor tekst-naar-spraak, die gebruikt kan worden om het gehele document, een specifieke pagina of de tekstselectie te lezen
* Achterwaartse richting voor te zoeken tekst
* Verbeterde ondersteuning voor formulieren
* Beginnende (echt basis en zeer waarschijnlijk onvolledige) ondersteuning voor JavaScript in PDF documenten
* Ondersteuning voor annotaties in bijgevoegd bestand
* Mogelijkheid om de witte rand van pagina's te verwijderen bij bekijken van pagina's
* Nieuwe backend voor EPub documenten
* Backend voor OpenDocument Text: ondersteuning voor versleutelde documenten, verbeteringen aan lijsten en tabellen
* XPS-backend: verschillende verbeteringen aan laden en rendering, zou nu veel bruikbaarder moeten zijn
* PS-backend: verschillende verbeteringen, hoofdzakelijk vanwege de vereiste nieuwere libspectre
