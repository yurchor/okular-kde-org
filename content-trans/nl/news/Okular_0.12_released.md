---
date: 2011-01-26
title: Okular 0.12 vrijgegeven
---
De 0.12 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.6. Deze vrijgave introduceert kleine reparaties en biedt extra mogelijkheden en wordt aanbevolen aan iedereen die Okular gebruikt.
