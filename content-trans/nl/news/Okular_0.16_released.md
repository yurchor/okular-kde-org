---
date: 2013-02-06
title: Okular 0.16 vrijgegeven
---
De 0.16 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.10. Deze vrijgave introduceert nieuwe functies zoals het zoomen naar hogere niveaus in PDF documenten, een op Active gebaseerde viewer voor tablets, meer ondersteuning voor PDF-films, gezichtsveld volgt de selectie en verbeteringen aan het bewerken van annotaties. Okular 0.16 wordt aanbevolen aan iedereen die Okular gebruikt.
