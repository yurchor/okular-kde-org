---
date: 2009-05-06
title: Okular 0.8.3 vrijgegeven
---
De derde onderhoudsuitgave van de KDE 4.2 serie bevat Okular 0.8.3. Het levert niet veel nieuws voor Okular, de enige relevante wijziging is over het bieden van meer veiligheid bij het maken paginaafbeeldingen van een XPS-document. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
