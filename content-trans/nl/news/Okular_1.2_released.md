---
date: 2017-08-17
title: Okular 1.2 vrijgegeven
---
De 1.2 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 17.08. Deze vrijgave introduceert zeer kleine reparaties en verbeteringen. U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 wordt aanbevolen aan iedereen die Okular gebruikt.
