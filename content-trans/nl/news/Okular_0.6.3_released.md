---
date: 2008-04-02
title: Okular 0.6.3 vrijgegeven
---
De derde onderhoudsuitgave van de KDE 4.0 serie bevat Okular 0.6.3. Het bevat reparaties van enige bugs, d.w.z. een betere manier om posities van tekst in een PDF-document te verkrijgen en enige reparaties voor systeem van annotaties en de inhoudsopgave. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
