---
faq:
- answer: Paket med Okular från Ubuntu (och därmed också Kubuntu är kompilerade utan
    stöd för dessa två format. Orsaken förklaras i [denna](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad-rapport.
  question: När Ubuntu används kan man inte läsa CHM- och EPub-dkoument, fastän okular-extra-backends
    och libchm1 är installerade. Varför?
- answer: Eftersom du inte har en text-till-tal tjänst på systemet, installera Qt:s
    talbibliotek så kommer de att aktiveras
  question: Varför är uppläsningsalternativen i verktygsmenyn gråa?
- answer: Installera paketet poppler-data
  question: 'Vissa tecken återges inte och när felsökning aktiveras anger vissa rader:
    ''Missing language pack for xxx'''
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Vanliga frågor
---
