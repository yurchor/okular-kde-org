---
date: 2007-07-10
title: Presentationsmaterial om Okular från aKademy 2007 på nätet
---
Föredraget om Okular som Pino Toscano höll på <a href="http://akademy2007.kde.org">aKademy 2007</a> finns nu på nätet. Både <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">stordiabilder</a> och <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a> är tillgängliga. 
