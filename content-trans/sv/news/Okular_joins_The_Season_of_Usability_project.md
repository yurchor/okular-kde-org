---
date: 2007-01-31
title: Okular går med i projektet Användbarhetens årstid (Season of Usability)
---
Okular-gruppen kungör med stolthet att Okular är ett av de program som har valts ut att delta i projektet <a href="http://www.openusability.org/season/0607/">Användbarhetens årstid (Season of Usability)</a>, skött av användbarhetsexperter på <a href="http://www.openusability.org">OpenUsability</a>. Därifrån välkomnar vi Sharad Baliyan till gruppen, och tackar Florian Graessle och Pino Toscano för deras pågående arbete med att förbättra Okular. 
