---
date: 2015-08-19
title: Okular 0.23 utgiven
---
Version 0.23 av Okular har givits ut tillsammans med utgåva 15.08 av KDE:s program. Utgåvan introducerar nya funktioner som latex-synctex omvänd sökning i DVI och mindre felrättningar. Okular 0.23 är en rekommenderad uppdatering för alla som använder Okular.
