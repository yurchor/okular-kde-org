---
date: 2020-04-23
title: Okular 1.10 utgiven
---
Version 1.10 av Okular har givits ut. Utgåvan introducerar kinetisk panorering, förbättringar av flikhantering, förbättringar av mobilt användargränssnitt och diverse mindre felrättningar och funktionsförbättringar överallt. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 är en rekommenderad uppdatering för alla som använder Okular.
