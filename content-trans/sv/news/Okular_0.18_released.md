---
date: 2013-12-12
title: Okular 0.18 utgiven
---
Version 0.18 av Okular har givits ut tillsammans med utgåva 4.12 av KDE:s program. Utgåvan introducerar nya funktioner som stöd för ljud och video i Epub-filer och dessutom förbättringar av befintliga funktioner som sökning och utskrift. Okular 0.18 är en rekommenderad uppdatering för alla som använder Okular.
