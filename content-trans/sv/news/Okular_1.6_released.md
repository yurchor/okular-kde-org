---
date: 2018-12-13
title: Okular 1.6 utgiven
---
Version 1.6 av Okular har givits ut tillsammans med utgåva 18.12 av KDE:s program. Utgåvan introducerar det nya kommentarverktyget Skrivmaskinstext bland diverse andra felrättningar och mindre funktionsförbättringar. Du kan titta på den fullständiga ändringsloggen på <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Okular 1.6 är en rekommenderad uppdatering för alla som använder Okular.
