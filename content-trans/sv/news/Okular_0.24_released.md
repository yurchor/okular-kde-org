---
date: 2015-12-16
title: Okular 0.24 utgiven
---
Version 0.24 av Okular har givits ut tillsammans med utgåva 15.12 av KDE:s program. Utgåvan introducerar mindre felrättningar och funktionalitet. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 är en rekommenderad uppdatering för alla som använder Okular.
