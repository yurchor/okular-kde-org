---
date: 2008-03-05
title: Okular 0.6.2 utgiven
---
Den tredje underhållsutgåvan i KDE 4.0-serien inkluderar Okular 0.6.2. Den innehåller en hel del felrättningar, inklusive bättre stabilitet när ett dokument stängs, och små rättningar av bokmärkessystemet. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
