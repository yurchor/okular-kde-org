---
date: 2008-09-03
title: Okular 0.7.1 utgiven
---
Den första underhållsutgåvan i KDE 4.1-serien inkluderar Okular 0.7.1. Den innehåller några kraschfixar bland andra mindre rättningar. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
