---
date: 2008-10-03
title: Okular 0.7.2 utgiven
---
Den andra underhållsutgåvan i KDE 4.1-serien inkluderar Okular 0.7.2. Den innehåller några mindre rättningar av TIFF- och Comicbook-gränssnitten och ändringen till "Fyll bredden" som förvald zoomnivå. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
