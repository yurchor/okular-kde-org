---
date: 2009-06-03
title: Okular 0.8.4 utgiven
---
Den fjärde underhållsutgåvan i KDE 4.2-serien inkluderar Okular 0.8.4. Den innehåller några fixar för OpenDocument textdokument, några kraschfixar, och ett fåtal mindre fel i gränssnittet. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
