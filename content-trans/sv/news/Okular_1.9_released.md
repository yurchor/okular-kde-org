---
date: 2019-12-12
title: Okular 1.9 utgiven
---
Version 1.9 av Okular har givits ut. Utgåvan introducerar stöd för cb7 serieboksarkiv, förbättrat Javascript-stöd för PDF-filer bland diverse andra felrättningar och mindre funktionsförbättringar. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 är en rekommenderad uppdatering för alla som använder Okular.
