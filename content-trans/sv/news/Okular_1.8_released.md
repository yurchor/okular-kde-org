---
date: 2019-08-15
title: Okular 1.8 utgiven
---
Version 1.8 av Okular har givits ut tillsammans med utgåva 19.08 av KDE:s program. Utgåvan introducerar en funktion för att ställa in radslut för kommentarer förutom diverse andra felrättningar och mindre funktionsförbättringar. Du kan titta på den fullständiga ändringsloggen på <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Okular 1.8 är en rekommenderad uppdatering för alla som använder Okular.
