---
date: 2010-02-09
title: Okular 0.10 utgiven
---
Version 0.10 av Okular har givits ut tillsammans med KDE:s programvarusamling 4..4. Förutom allmänna förbättringar av stabiliteten, ståtar utgåvan med nytt, förbättrat stöd för både omvänd sökning och sökning framåt som länkar källkodsrader i Latex med motsvarande platser i dvi- och pdf-filer.
