---
date: 2018-08-16
title: Okular 1.5 utgiven
---
Version 1.5 av Okular har givits ut tillsammans med utgåva 18.08 av KDE:s program. Utgåvan introducerar förbättringar av Formulär bland diverse andra felrättningar och mindre funktionsförbättringar. Du kan titta på den fullständiga ändringsloggen på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 är en rekommenderad uppdatering för alla som använder Okular.
