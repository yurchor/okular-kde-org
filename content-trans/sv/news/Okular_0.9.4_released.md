---
date: 2009-12-01
title: Okular 0.9.4 utgiven
---
Den fjärde underhållsutgåvan i KDE 4.3-serien inkluderar Okular 0.9.4. Den innehåller några kraschfixar, och ett fåtal mindre felrättningar av gränssnittet. Man kan läsa om alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
