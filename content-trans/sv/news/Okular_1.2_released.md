---
date: 2017-08-17
title: Okular 1.2 utgiven
---
Version 1.2 av Okular har givits ut tillsammans med utgåva 17.08 av KDE:s program. Utgåvan introducerar mindre felrättningar och förbättringar. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 är en rekommenderad uppdatering för alla som använder Okular.
