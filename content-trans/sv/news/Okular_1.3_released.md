---
date: 2017-12-14
title: Okular 1.3 utgiven
---
Version 1.3 av Okular har givits ut tillsammans med utgåva 17.12 av KDE:s program. Utgåvan introducerar ändringar av hur spara kommentarer och formulärdata fungerar, lägger till stöd för partiell återgivning av uppdateringar för filer som tar lång tid att återge, gör textlänkar interaktiva i textmarkeringsläge, lägger till alternativet Dela i arkivmenyn, lägger till stöd för Markdown och rättar några fel rörande stöd för hög upplösning. Du kan titta på den fullständiga ändringsloggen på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 är en rekommenderad uppdatering för alla som använder Okular.
