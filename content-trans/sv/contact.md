---
menu:
  main:
    parent: about
    weight: 4
title: Kontakter
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, KDE:s maskot" style="height: 200px;"/>

Man kan kontakta Okular-gruppen på många sätt:

* E-postlistor: För att koordingera utvecklingen av Okular, använder vi [e-postlistan okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) på kde.org. Den kan användas för att diskutera utvecklingen av huvudprogrammet, men återmatning om befintliga eller nya gränssnitt uppskattas också.

* IRC: För allmänt chatt använder vi IRC [#okular](irc://irc.kde.org/#okular) och [#kde-devel](irc://irc.kde.org/#kde-devel) på [Freenode-nätverket](http://www.freenode.net/). En del av Okulars utvecklare kan hittas hängande där.

* Matrix: Den tidigare nämnda chatten kan också kommas åt med Matrix-nätverket via [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum: Om du föredrar att använda ett forum, kan du använda [Okulars forum](http://forum.kde.org/viewforum.php?f=251) inne i det större [KDE:s gemenskapsforum](http://forum.kde.org/).

* Fel och önskemål: Fel och önskemål ska rapporteras till [KDE:s felspårare](https://bugs.kde.org/). Om du vill  hjälpa till, hittar du en lista med de viktigaste felen [här](https://community.kde.org/Okular).
