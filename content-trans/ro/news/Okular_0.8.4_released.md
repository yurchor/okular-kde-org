---
date: 2009-06-03
title: Okular 0.8.4 lansat
---
The forth maintenance release of the KDE 4.2 series includes Okular 0.8.4. It includes some fixes in OpenDocument Text documents, a couple of crash fixes, and few small bugs in the interface. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
