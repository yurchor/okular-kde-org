---
menu:
  main:
    name: Izgradi ga
    parent: about
    weight: 2
title: Prevod Okularja iz izvorne kode na Linuxu
---
<span style="background-color:#e8f4fa">Če iščete prevedene pakete, obiščite [download page](/download/). Stanje prevoda lahko preverite [tukaj](https://repology.org/project/okular/versions)</span>

Če želite prevesti Okular, morate imeti nastavljeno okolje za prevajanje, ki ga mora na splošno zagotavljati vaša distribucija. Če želite sestaviti razvojno različico Okularja, glejte "Build from source" na Wiki-ju skupnosti KDE.

Okular lahko prenesete in prevedete na ta način:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Če namestite Okular na drugačno pot kot v sistemski namestitveni imenik, je možno, da morate zagnati `source build/prefix.sh; okular`, da se uporabi pravi pojavek Okular in knjižnice.

## Neobvezni paketi

Obstaja nekaj neobveznih paketov, ki jih lahko namestite, da bi bilo v Okularju še nekaj dodatnih funkcij. Nekateri so morda že zapakirani za vašo distribucijo, drugi pa ne. Če se želite izogniti težavam, se držite paketov, ki jih podpira vaša distribucija

* Poppler (ozadje PDF): Za prevod ozadja PDF, potrebujete [knjižnico Poppler](http://poppler.freedesktop.org), za katero je zahtevana vsaj verzija 0.24
* Libspectre: Če želite sestaviti in uporabiti to zaledje PostScipt (PS), potrebujete libspectre>= 0.2. Če ga paketa ni v vaši distribuciji ali pakirana različica ni dovolj, ga lahko prenesete [tukaj] (http://libspectre.freedesktop.org)
* DjVuLibre: Če želite sestaviti zaledje DjVu, potrebujete DjVuLibre>= 3.5.17. Tako kot pri Libspectre, jo lahko dobite v svojem distribucijskem sistemu ali [tukaj] (http://djvulibre.djvuzone.org).
* libTIFF: Ta je potreben za podporo TIFF/faksa. Trenutno ni nobene minimalno zahtevane različice, zato bi morala delovati katera koli najnovejša različica knjižnice, ki je na voljo v vašem distro. V primeru težav se obrnite na razvijalce Okularja.
* libCHM: To je potrebno za prevod zaledja CHM. Tako kot pri libTIFF tudi tukaj ni zahteve po minimalni različici
* Libepub: Če potrebujete podporo za EPub, lahko to knjižnico namestite iz distribucije ali iz [sourceforge](http://sourceforge.net/projects/ebook-tools).
