---
intro: Okular podpira široko izbor formatov dokumentov in primerov uporabe. Ta stran
  se vedno nanaša na stabilno serijo Okular, trenutno je to Okular 20.12
layout: formats
menu:
  main:
    name: Format dokumenta
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status upravljavcev formatov dokumenta
---
