---
date: 2007-01-31
title: Okular se pridruži Projektu uporabnosti (The Season of Usability project)
---
Ekipa Okular team s ponosom objavlja, da je bil izbran Okular kot ena od aplikacij v projektu <a href="http://www.openusability.org/season/0607/">Season of Usability</a> projekt, ki ga vodijo eksperti za uporabnost pri <a href="http://www.openusability.org">OpenUsability</a>. Od tam želimo dobrodošlico Sharad Baliyan v ekipi in se zahvaljujemo Florian Graessle in Pino Toscano za njun stalni napor pri izboljšavah programa Okular.
