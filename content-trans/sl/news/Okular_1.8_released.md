---
date: 2019-08-15
title: Izšel je Okular 1.8
---
Različica Okular 1.8 je bila izdana skupaj s KDE Applications 19.08. Ta izdaja med različnimi popravki in manjšimi dodatnimi funkcijami uvaja možnost nastavitve končnic vrstičnih zaznamkov. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Okular 1.8 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
