---
date: 2008-04-02
title: Izšel je Okular 0.6.3
---
Tretja izdaja vzdrževanja serije KDE 4.0 vključuje Okular 0.6.3. Vključuje nekaj popravkov napak, tj. boljši način, da dobite položaj besedila v dokumentu PDF, in nekaj popravkov za sistem zaznamkov ter kazalo. Vse odpravljene težave lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
