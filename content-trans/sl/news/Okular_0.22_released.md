---
date: 2015-04-15
title: Izšel je Okular 0.22
---
Različna različica programa Okular je bila izdana skupaj s programi KDE izdaja 15.04. Okular 0.22 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
