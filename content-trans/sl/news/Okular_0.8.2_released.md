---
date: 2009-04-02
title: Izšel je Okular 0.8.2
---
Druga izdaja za vzdrževanje serije KDE 4.2 vključuje Okular 0.8.2. Vključuje boljšo podporo (upamo, da deluje) za DVI in pdfsync inverznoiskanje in popravke ter majhne izboljšave v predstavitvenem načinu. Vse odpravljene težave si lahko preberete na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
