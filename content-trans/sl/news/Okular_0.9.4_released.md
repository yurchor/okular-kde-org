---
date: 2009-12-01
title: Izšel je Okular 0.9.4
---
Četrta vzdrževalna izdaja serije KDE 4.3 vključuje Okular 0.9.4. Vključuje nekaj popravkov izpadov programa in nekaj majhnih popravkov napak v vmesniku. Vse odpravljene težave lahko preberete na <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
