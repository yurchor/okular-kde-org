---
date: 2015-12-16
title: Izšel je Okular 0.24
---
Različica Okular 0.24 je bila izdana skupaj s KDE Applications 15.12. Ta izdaja uvaja manjše popravke in funkcije, lahko jih preverite na celotnem dnevniku sprememb na <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
