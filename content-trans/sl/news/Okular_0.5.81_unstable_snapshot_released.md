---
date: 2006-11-02
title: Izšel je Okular 0.5.81 kot nestabilni prikaz
---
Ekipa Okular s ponosom objavlja izdajo delovne verzije programa Okular, ki se prevaja skupaj z <a href="http://dot.kde.org/1162475911/">Drugo razvojno verzijo KDE 4</a>. Ta verzija še ni v popolnosti funkcionalna, saj imamo še veliko reči za zgladiti in zaključiti, a lahko jo že preizkusite in nam pošljete kolikor povratnih informacij želite. Delovni paket lahko najdete na <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Poglejte na stran <a href="download.php">download</a>, da ste prepričani, da imate vse potrebne knjižnice.
