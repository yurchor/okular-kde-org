---
date: 2008-02-05
title: Izšel je Okular 0.6.1
---
Prva izdaja za vzdrževanje serije KDE 4.0 vključuje Okular 0.6.1. Vključuje kar nekaj popravkov napak, vključno z boljšim ponovnim nalaganjem datotek pri shranjevanju, izboljšavami uporabnosti itd. Vse odpravljene težave lahko preberete na <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
