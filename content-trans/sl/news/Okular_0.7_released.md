---
date: 2008-07-29
title: Izšel je Okular 0.7
---
Skupina Okular s ponosom napoveduje novo različico Okularja, ki je bila izdana kot del <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Nekatere nove funkcije in izboljšave vključujejo (v naključnem vrstnem redu):

* Možnost shranjevanja dokumenta PDF s spremembami v poljih obrazca
* Predstavitveni način: podpora za več kot en zaslon in možnost onemogočenja prehodov
* Raven povečave je zdaj shranjena v dokumentu
* Izboljšana podpora za pretvorbo besedila v govor, ki se lahko uporablja za branje celotnega dokumenta, določene strani ali izbire besedila
* Iskanje besedila v povratni smeri
* Izboljšana podpora za obrazce
* Predhodna (res osnovna in najverjetneje nepopolna) podpora za JavaScript v dokumentih PDF
* Podpora za zaznamke v prilogah datotek
* Možnost odstranitve bele obrobe strani pri ogledu strani
* Nov zaledje za dokumente EPub
* Zaledje besedila OpenDocument: podpora za izboljšave šifriranih dokumentov, seznamov in tabel
* Zaledje XPS: različne izboljšave nalaganja in upodabljanja bi morale biti zdaj veliko bolj uporabne
* Zaledje PS: različne izboljšave, predvsem zaradi zahtevane novejše libspectre
