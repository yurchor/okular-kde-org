---
date: 2008-09-03
title: Izšel je Okular 0.7.1
---
Prva izdaja za vzdrževanje serije KDE 4.1 vključuje Okular 0.7.1. Med drugimi manjšimi popravki vključuje nekatere popravke izpadov programa. Vse odpravljene težave lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
