---
date: 2017-04-20
title: Izšel je Okular 1.1
---
Različica Okular 1.1 je bila izdana skupaj s KDE Applications 17.04. Ta izdaja uvaja funkcionalnost spreminjanja velikosti zaznamkov, podporo za samodejni izračun vsebine obrazca, izboljšave zaslona na dotik in še več! Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 je apriporočena posodobitev za vse, ki uporabljajo Okular.
