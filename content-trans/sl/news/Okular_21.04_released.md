---
date: 2021-04-22
title: Izšel je Okular 21.4
---
Izšla je različica Okular za sistem 21.04. Ta izdaja uvaja digitalno podpisovanje datotek PDF ter različne manjše popravke in funkcije. Celoten dnevnik sprememb lahko preverite na <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. Okular 21.04 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
