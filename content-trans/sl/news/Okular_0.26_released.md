---
date: 2016-08-18
title: Izšel je Okular 0.26
---
Različica Okular 0.26 je bila izdana skupaj s KDE Applications 16.08. Ta izdaja uvaja samo majhne spremembe. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
