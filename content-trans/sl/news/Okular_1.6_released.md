---
date: 2018-12-13
title: Izšel je Okular 1.6
---
Različica 1.6 Okular je bila izdana skupaj s KDE Applications 18.12. Ta izdaja med različnimi drugimi popravki in manjšimi dodatnimi funkcijami uvaja novo orodje za opombe pisalnega stroja. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Okular 1.6 je priporočena posodobitev za vse, ki uporabljajo Okular.
