---
date: 2020-08-13
title: Izšel je Okular 1.11
---
Izšla je različica programa Okular 1.11. Ta izdaja uvaja nov uporabniški vmesnik za opombe ter različne manjše popravke in funkcije. Celoten dnevnik sprememb lahko preverite na <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a> Okular 1.11 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
