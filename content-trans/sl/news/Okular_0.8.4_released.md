---
date: 2009-06-03
title: Izšel je Okular 0.8.4
---
Četrta izdaja vzdrževanja serije KDE 4.2 vključuje Okular 0.8.4. Vključuje nekaj popravkov v dokumentih OpenDocument Text, nekaj popravkov zrušitev in nekaj majhnih napak v vmesniku. Vse popravljene težave si lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
