---
faq:
- answer: Ubuntu (in tudi Kubuntu) paketi programa Okular so prevedeni brez podpore
    za ta dva formata. Razlog je opisan v [tem](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad poročilu.
  question: Ko uporabljam Ubuntu, ne morem brati dokumentov CHM in EPub, tudi če imam
    nameščene okular-extra-backends in libchm1. Zakaj?
- answer: Ker v vašem sistemu ni nameščenih govornih storitev, namestite Qt Speech
    library in bi morale biti omogočene
  question: Zakaj so možnosti govora v meniju Orodja osivele?
- answer: Namestite paket poppler-data
  question: Nekateri znaki niso upodobljeni in pri omogočanju odpravljanja napak nekatere
    vrstice omenjajo 'Manjka jezikovni paket za xxx'
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Pogosto zastavljena vprašanja
---
