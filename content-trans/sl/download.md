---
intro: Okular je na voljo v vnaprej pripravljenem paketu na številnih platformah.
  Na desni lahko preverite stanje paketa za vaš distribucijski sistem Linux ali nadaljujete
  z branjem informacij o drugih operacijskih sistemih
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular je že na voljo v večini distribucij Linuxa. Namestite ga lahko iz [KDE
    Software Center](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpak logo
  name: Flatpak
  text: Najnovejšo različico [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    lahko namestite s strani Flathub. Eksperimentalne Flatpake z nočnimi gradnjami
    Okularja lahko [namestite iz repozitorija KDE Flatpak](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications).
- image: /images/ark.svg
  image_alt: Ark logo
  name: Izdaje izvorne kode
  text: Okular se redno izdaja kot del aplikacij KDE. Če ga želite izgraditi iz izvorne
    kode, lahko preverite razdelek [Build It](/build-it).
- image: /images/windows.svg
  image_alt: Windows logo
  name: Windows
  text: Za Windows si oglejte spletno stran <a href='https://community.kde.org/Windows'>KDE
    v programu Windows Initiative</a> za informacije o namestitvi programske opreme
    KDE v sistem Windows. Stabilna izdaja je na voljo v trgovini <a href='https://www.microsoft.com/store/apps/9N41MSQ1WNM8'>Microsoft
    Store</a>. Obstajajo tudi <a href='https://binary-factory.kde.org/job/Okular_Nightly_win64/'>poskusni
    nočni strojni prevodi</a>. Preizkusite in pošljite popravke.
sassFiles:
- /sass/download.scss
title: Prenos
---
