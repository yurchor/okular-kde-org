---
menu:
  main:
    parent: about
    weight: 4
title: Stik
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, maskota KDE" style="height: 200px;"/>

Do ekipe Okularja lahko pridete v stik na več načinov:

* Poštni seznam: Za usklajevanje razvoja okularja uporabimo [poštni seznam okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) na kde.org. Z njim se lahko pogovarjate o razvoju osnovne aplikacije in pošiljate povratne informacije o obstoječih ali novih zalednih sistemih.

* IRC: Za splošni klepet uporabljamo IRC [#okular](irc://irc.kde.org/#okular) in [#kde-devel](irc://irc.kde.org/#kde-devel) v omrežju [Freenode network](http://www.freenode.net/). Tam lahko najdete nekatere razvijalce Okular.

* Matrix: Do prej omenjenega klepeta je mogoče dostopati tudi preko omrežja Matrix [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Če vam je ljubši forum, lahko uporabite [Okular forum](http://forum.kde.org/viewforum.php?f=251) znotraj večjega [Foruma skupnosti KDE](http://forum.kde.org/)

* Napake in želje: O napakah in željah je treba obvestiti sledilnik napak [KDE bug tracker](https://bugs.kde.org/). Če želite pomagati, lahko najdete seznam najpogostejših napak [tukaj](https://community.kde.org/Okular).
