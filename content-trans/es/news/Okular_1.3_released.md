---
date: 2017-12-14
title: Okular 1.3 publicado
---
La versión 1.3 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 17.12. Esta versión introduce cambios en cómo se guardan las anotaciones y los datos de los formularios, permite la visualización parcial de archivos que necesitan mucho tiempo para visualizarse, hace que los enlaces de texto sean interactivos en el modo de selección de texto, añade una opción «Compartir» en el menú «Archivo», añade visualización de Markdown y corrige algunos problemas relacionados con el uso de HiDPI. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 es una actualización recomendada para todos los usuarios de Okular.
