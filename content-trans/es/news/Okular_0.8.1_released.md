---
date: 2009-03-04
title: Okular 0.8.1 publicado
---
El primer lanzamiento de mantenimiento de la serie KDE 4.2 incluye Okular 0.8.1. Incluye algunos arreglos de estabilidad en los motores de CHM y DjVu, y cambios menores en la interfaz de usuario.Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4__2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4__2to4_2_1.php</a>
