---
date: 2015-12-16
title: Lanzamiento de Okular 0.24
---
La versión 0.24 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 15.11. Esta versión contiene correcciones de errores y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 es una actualización recomendada para todos los usuarios de Okular.
