---
date: 2020-12-10
title: Okular 20.12 publicado
---
Se ha publicado la versión 20.12 de Okular. Esta versión introduce varias correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 es una actualización recomendada para todos los usuarios de Okular.
