---
date: 2015-04-15
title: Okular 0.22 publicado
---
La versión 0.22 de Okular ha sido publicada junto con el lanzamiento de las aplicaciones de KDE 15.04. Okular 0.22 es una actualización recomendada para todos los usuarios de Okular.
