---
date: 2015-08-19
title: Lanzamiento de Okular 0.23
---
Junto a las aplicaciones de KDE 15.08, se ha lanzado la versión 0.23 de Okular. Esta versión incluye la implementación de la transición de sombreado en el modo de presentación, así como algunas soluciones de errores en las anotaciones y en la reproducción de vídeo.  Okular 0.23 es una actualización recomendada para todos los usuarios de Okular.
