---
date: 2009-06-03
title: Okular 0.8.4 publicado
---
El cuarto lanzamiento de la serie KDE 4.2 incluye Okular 0.8.4. Incluye algunos arreglos en documentos OpenDocument Text, y algunos cambios pequeños a la interfaz. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_3_4.php</a>
