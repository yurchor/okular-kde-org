---
date: 2011-01-26
title: Okular 0.12 publicado
---
La versión 0.12 de Okular ha sido publicada junto con el lanzamiento de las aplicaciones de KDE 4.6. Esta versión incluye correcciones y características menores, y es una actualización recomendada para todos los usuarios de Okular.
