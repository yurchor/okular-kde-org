---
date: 2017-04-20
title: Okular 1.1 publicado
---
La versión 1.1 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 17.04. Esta versión introduce la función de cambiar el tamaño de las anotaciones, el cálculo automático del contenido de los formularios y mejoras para pantallas táctiles, entre otras. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 es una actualización recomendada para todos los usuarios de Okular.
