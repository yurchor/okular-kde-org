---
date: 2007-04-04
title: Okular en kdegraphics
---
El equipo de Okular se enorgullece de anunciar que Okular ya forma parte del módulo kdegraphics. La próxima versión de Okular estará incluida con KDE 4.0.
