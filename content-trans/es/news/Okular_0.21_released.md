---
date: 2014-12-17
title: Okular 0.21 publicado
---
La versión 0.21 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 14.12. Esta versión incluye nuevas funcionalidades, como la búsqueda hacia atrás latex-synctex en dvi, así como soluciones de pequeños errores. Okular 0.21 es una actualización recomendada para todos los usuarios de Okular.
