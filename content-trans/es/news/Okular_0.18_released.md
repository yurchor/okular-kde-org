---
date: 2013-12-12
title: Okular 0.18 publicado
---
La versión 0.18 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 4.12. Esta versión incluye nuevas funcionalidades como la reproducción de audio y vídeo para el formato ePub y también mejoras para las funcionalidades existentes como la de búsqueda e impresión. Okular 0.18 es una actualización recomendada para todos los usuarios de Okular.
