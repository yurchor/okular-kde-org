---
date: 2016-08-18
title: Lanzamiento de Okular 0.26
---
La versión 0.26 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 16.08. Esta versión contiene muy pocos cambios. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 es una actualización recomendada para todos los usuarios de Okular.
