---
date: 2016-12-15
title: Okular 1.0 publicado
---
La versión 1.0 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 16.12. Esta versión está basada en KDE Frameworks 5. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 es una actualización recomendada para todos los usuarios de Okular.
