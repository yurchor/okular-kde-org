---
menu:
  main:
    name: Compilación
    parent: about
    weight: 2
title: Compilación del código fuente de Okular en Linux
---
<span style="background-color:#e8f4fa">Si está buscando paquetes precompilados, visite la [página de descarga](/download/). Puede comprobar el estado de empaquetado [aquí](https://repology.org/project/okular/versions).</span>

Si quiere compilar Okular, debe disponer de un entorno de compilación previamente configurado, algo que generalmente debe proporcionar su distribución. En el caso de que desee compilar la versión de desarrollo de Okular, consulte cómo compilar a partir del código fuente en la Wiki de la Comunidad de KDE.

Puede descargar y compilar Okular de este modo:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Si instala Okular en una ruta distinta al directorio de instalación de su sistema, es posible que necesite ejecutar `source build/prefix.sh; okular` para que se escojan las instancia y bibliotecas correctas de Okular.

## Paquetes opcionales

Existen varios paquetes opcionales que puede instalar para que Okular tenga más funcionalidades. Algunos de ellos ya los proporciona su distribución, aunque es posible que otros no. Si quiere evitar posibles problemas, use solo los paquetes que proporciona su distribución.

* Poppler (motor PDF): Para compilar el motor PDF necesitará [la biblioteca Poppler](http://poppler.freedesktop.org), para la que se requiere al menos la versión 0.24.
* Libspectre: Para compilar y usar este motor PostScript (PS), necesita libspectre >= 0.2. Si su distribución no lo distribuye, o la versión empaquetada no es suficiente, puede descargarla de [aquí](http://libspectre.freedesktop.org).
* DjVuLibre: Para compilar el motor DjVu, necesita DjVuLibre >= 3.5.17. Al igual que Libspectre, se puede obtener de su distribución o de [aquí](http://djvulibre.djvuzone.org).
* libTIFF: Necesario para TIFF/fax. En la actualidad no se requiere ninguna versión mínima, por lo que debería funcionar cualquier versión reciente de la biblioteca proporcionada por su distribución. En caso de encontrar algún problema, no dude en contactar con los desarrolladores de Okular.
* libCHM: Necesaria para compilar el motor CHM. Como con libTIFF, no existe ningún requisito de versión mínima.
* Libepub: Si necesita usar EPub, puede instalar esta biblioteca desde su distribución o desde [sourceforge](http://sourceforge.net/projects/ebook-tools).
