---
faq:
- answer: Los paquetes de Okular que proporciona Ubuntu (así como Kubuntu) están compilados
    sin implementar estos dos formatos. El motivo se explica en [este](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    informe de Launchpad.
  question: Cuando uso Ubuntu, no puedo leer documentos CHM ni EPub, incluso si tengo
    instalados okular-extra-backends y libchm1. ¿Por qué?
- answer: Porque no tiene instalado un servicio de habla en su sistema. Instale la
    biblioteca de habla de Qt para activarlo.
  question: ¿Por qué aparecen deshabilitadas las opciones de habla del menú Herramientas?
- answer: Instale el paquete «poppler-data».
  question: Algunos caracteres no se muestran y, cuando se activa la depuración, en
    algunas líneas pone «Falta el paquete de idioma para xxx».
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Preguntas frecuentes
---
