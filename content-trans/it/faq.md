---
faq:
- answer: I pacchetti di Okular per Ubuntu (anche per Kubuntu) sono stati compilati
    senza il supporto per questi due formati. La ragione è spiegata in [questo](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    report Launchpad.
  question: Quando uso Ubuntu non posso leggere documenti CHM ed EPub anche se ho
    installato okular-extra-backends e libchm1. Perché?
- answer: Perché non è presente un servizio di pronuncia vocale nel tuo sistema. Installa
    la libreria Qt Speech e le opzioni dovrebbero abilitarsi
  question: Perché le opzioni della pronuncia nel menu degli Strumenti sono in grigio?
- answer: Installa il pacchetto poppler-data
  question: Alcuni caratteri non vengono resi e quando si abilita il debug alcune
    righe menzionano 'Missing language pack for xxx'
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Domande ricorrenti
---
