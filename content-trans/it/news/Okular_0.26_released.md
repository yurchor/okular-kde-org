---
date: 2016-08-18
title: Rilasciato Okular 0.26
---
La versione 0.26 di Okular è stato rilasciata insieme con le applicazioni di KDE 16.08. Questo rilascio introduce pochissime modifiche, puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
