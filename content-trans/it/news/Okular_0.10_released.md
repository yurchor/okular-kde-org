---
date: 2010-02-09
title: Rilasciato Okular 0.10
---
La versione 0.10 di Okular è stata rilasciata insieme con KDE SC 4.4. Oltre che al miglioramento generale della stabilità, questa nuova versione offre supporto migliorato sia per la ricerca inversa che in avanti, le quali collegano le righe del codice sorgente in LaTeX con le posizioni corrispondenti nei file dvi e pdf.
