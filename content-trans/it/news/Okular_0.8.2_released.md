---
date: 2009-04-02
title: Rilasciato Okular 0.8.2
---
Il secondo rilascio di manutenzione della serie KDE 4.2 comprende Okular 0.8.2. Include un supporto migliore (si spera funzionante) per la ricerca inversa di DVI e pdfsync, varie correzioni e piccoli miglioramenti in modalità presentazione. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
