---
date: 2020-08-13
title: Rilasciato Okular 1.11
---
È stata rilasciata la versione 1.11 di Okular. Questo rilascio introduce una nuova interfaccia utente per le annotazioni, oltre a varie correzioni minori e aggiunte di funzionalità dappertutto. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
