---
date: 2008-03-05
title: Rilasciato Okular 0.6.2
---
Il secondo rilascio di manutenzione della serie KDE 4.0 comprende Okular 0.6.2. Include alcune correzioni errori, inclusa una maggiore stabilità nella chiusura di un documento e piccole correzioni nel sistema dei segnalibri. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
