---
date: 2013-08-16
title: Rilasciato Okular 0.17
---
La versione 0.17 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.11. Questo rilascio introduce nuove funzionalità come il supporto annulla/rifai per i moduli e le annotazioni e strumenti configurabili per la revisione. È un aggiornamento consigliato per chiunque utilizzi Okular.
