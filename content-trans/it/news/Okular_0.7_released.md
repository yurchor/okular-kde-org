---
date: 2008-07-29
title: Rilasciato Okular 0.7
---
La squadra di Okular è orgogliosa di annunciare una nuova versione di Okular, rilasciata come parte di <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Alcune delle nuove funzionalità e miglioramenti includono (in ordine casuale):

* Possibilità di salvare un documento PDF con le modifiche ai campi dei moduli
* Modo presentazione: supporto per più di uno schermo e possibilità di disabilitare le transizioni
* Il livello di ingrandimento ora è memorizzato per documento
* Supporto alla sintesi vocale migliorato, che può essere utilizzato per leggere l'intero documento, una pagina specifica o una selezione di testo
* Direzione inversa per la ricerca di testo
* Supporto migliorato per i moduli
* Supporto preliminare (davvero di base e molto probabilmente incompleto) per JavaScript nei documenti PDF
* Supporto per le annotazioni dei file allegati
* Possibilità di rimuovere il bordo bianco delle pagine durante la visualizzazione
* Nuovo motore per i documenti EPub
* Motore OpenDocument Text: supporto per i documenti cifrati, miglioramenti per gli elenchi e le tabelle
* Motore XPS: miglioramenti sulla resa e il caricamento, ora dovrebbe essere molto più fruibile
* Motore PS: miglioramenti vari, principalmente dovuto all'uso della nuova libreria libspectre
