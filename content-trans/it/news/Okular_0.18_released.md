---
date: 2013-12-12
title: Rilasciato Okular 0.18
---
La versione 0.18 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.12. Questo rilascio introduce nuove funzionalità come il supporto audio/video nei file EPub e miglioramenti a caratteristiche esistenti, quali la ricerca e la stampa. È un aggiornamento consigliato per chiunque utilizzi Okular.
