---
date: 2016-12-15
title: Rilasciato Okular 1.0
---
La versione 1.0 di Okular è stato rilasciata insieme con le applicazioni di KDE 16.12. Questo rilascio si basa ora su KDE Frameworks 5, puoi verificare tutte le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
