---
date: 2006-11-02
title: Rilasciata la versione Okular 0.5.81 non stabile
---
La squadra di Okular è orgogliosa di annunciare  il rilascio di una versione di prova di Okular che si compila nel <a href="http://dot.kde.org/1162475911/">Second KDE 4 Developers Snapshot</a>. Questa versione non è ancora completamente funzionale, poiché abbiamo parecchie cose da raffinare e finire, ma siete liberi di provarlo e fornire un feedback quanto si vuole. Potete trovare la versione di prova in <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Date un'occhiata alla pagina di <a href="download.php">download</a> per essere sicuri di avere tutte le librerie necessarie.
