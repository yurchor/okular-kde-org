---
date: 2007-01-31
title: Okular entra a far parte del progetto Season of Usability
---
La squadra di Okular è orgogliosa di annunciare che Okular è stata una delle applicazioni selezionate a partecipare al progetto <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, gestito da esperti di usabilità di <a href="http://www.openusability.org">OpenUsability</a>. Vogliamo dare il benvenuto a Sharad Baliyan e ringraziare Florian Graessle e Pino Toscano per il loro continuo lavoro nel suo miglioramento.
