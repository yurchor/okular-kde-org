---
date: 2009-03-04
title: Rilasciato Okular 0.8.1
---
Il primo rilascio di manutenzione della serie KDE 4.2 comprende Okular 0.8.1. Include alcune correzioni dei crash dei backend CHM e DjVu e correzioni minori dell'interfaccia utente. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
