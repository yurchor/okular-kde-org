---
date: 2017-04-20
title: Rilasciato Okular 1.1
---
La versione 1.1 di Okular è stato rilasciata insieme con le applicazioni di KDE 17.04. Questo rilascio introduce la funzione di ridimensionamento delle annotazioni, il supporto per il calcolo automatico del contenuto delle forme, miglioramenti allo schermo tattile e altro ancora. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
