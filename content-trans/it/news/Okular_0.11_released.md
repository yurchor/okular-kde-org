---
date: 2010-08-10
title: Rilasciato Okular 0.11
---
La versione 0.11 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.5. Questo rilascio introduce piccole correzioni e funzionalità ed è un aggiornamento consigliato per chiunque utilizzi Okular.
