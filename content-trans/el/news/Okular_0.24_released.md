---
date: 2015-12-16
title: Κυκλοφορία της έκδοσης 0.24 του Okular
---
Η έκδοση 0.24 του Okular κυκλοφόρησε μαζί με την έκδοση 15.12 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει μικρές διορθώσεις και χαρακτηριστικά και μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 0.24.
