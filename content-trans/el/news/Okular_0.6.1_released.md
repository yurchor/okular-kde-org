---
date: 2008-02-05
title: Κυκλοφορία της έκδοσης 0.6.1 του Okular
---
Η πρώτη έκδοση συντήρησης της σειράς 4.0 του KDE περιλαμβάνει το Okular 0.6.1 με αρκετές διορθώσεις σφάλματος, με λιγότερες επαναλήψεις στη λήψη αρχείων κατά την αποθήκευση, χρηστικές βελτιώσεις κλπ. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
