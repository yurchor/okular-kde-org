---
date: 2009-04-02
title: Κυκλοφορία της έκδοσης 0.8.2 του Okular
---
Η δεύτερη έκδοση συντήρησης της σειράς 4.2 του KDE περιλαμβάνει το Okular 0.8.2 με καλύτερη υποστήριξη (ελπίζουμε ότι λειτουργεί) για αντίστροφη αναζήτηση σε DVI και pdfsync και με διορθώσεις και μικρο-βελτιώσεις στη λειτουργία των παρουσιάσεων. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
