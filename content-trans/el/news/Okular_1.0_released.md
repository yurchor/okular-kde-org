---
date: 2016-12-15
title: Κυκλοφορία της έκδοσης 1.0 του Okular
---
Η έκδοση 1.0 του Okular κυκλοφόρησε μαζί με την έκδοση 16.12 του πακέτου KDE Applications. Η έκδοση αυτή βασίζεται στο KDE Frameworks 5 και μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.0.
