---
date: 2017-04-20
title: Κυκλοφορία της έκδοσης 1.1 του Okular
---
Η έκδοση 1.1 του Okular κυκλοφόρησε μαζί με την έκδοση 17.04 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει τη λειτουργία αλλαγής μεγέθους σχολιασμών, υποστηρίζει τον αυτόματο υπολογισμό των περιεχομένων φορμών, βελτιώσεις για οθόνες αφής και πολλά άλλα! Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.1.
