---
date: 2011-07-27
title: O Okular 0.13 foi lançado
---
A versão 0.13 do Okular foi lançada em conjunto com a versão 4.7 das Aplicações do KDE. Esta versão introduz algumas pequenas correcções e funcionalidades, e é uma actualização recomendada para todos os que usam o Okular.
