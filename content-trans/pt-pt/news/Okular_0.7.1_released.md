---
date: 2008-09-03
title: O Okular 0.7.1 foi lançado
---
A primeira versão de manutenção da série KDE 4.1 inclui o Okular 0.7.1. Inclui algumas correcções de estoiros, assim como algumas pequenas afinações. Poderá ver todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
