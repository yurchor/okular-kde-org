---
date: 2008-11-04
title: O Okular 0.7.3 foi lançado
---
A terceira versão de manutenção da série KDE 4.1 inclui o Okular 0.7.3. Este inclui algumas correcções menores na interface do utilizador e na pesquisa de texto. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
