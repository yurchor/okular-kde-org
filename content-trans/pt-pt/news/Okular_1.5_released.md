---
date: 2018-08-16
title: O Okular 1.5 foi lançado
---
A versão 1.5 do Okular foi lançada em conjunto com a versão 18.08 das Aplicações do KDE. Esta versão introduz melhorias nos formulários, entre outras pequenas correcções de erros e funcionalidades. Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=1.08.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
