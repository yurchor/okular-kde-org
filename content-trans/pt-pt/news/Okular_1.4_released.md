---
date: 2018-04-19
title: O Okular 1.4 foi lançado
---
A versão 1.4 do Okular foi lançada em conjunto com a versão 18.04 das Aplicações do KDE. Esta versão introduz um suporte para JavaScript melhorado nos ficheiros PDF e suporta o cancelamento de desenho do PDF, o que significa que se tiver um ficheiro PDF complexo e queira mudar a ampliação, enquanto está a desenhar, irá cancelar automaticamente em vez de esperar que o desenho termine. Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. O Okular 1.4 é uma actualização recomendada para todos os que usam o Okular.
