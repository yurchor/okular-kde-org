---
date: 2017-08-17
title: O Okular 1.2 foi lançado
---
A versão 1.2 do Okular foi lançada em conjunto com a versão 17.08 das Aplicações do KDE. Esta versão introduz pequenas correcções de erros e funcionalidades, podendo verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
