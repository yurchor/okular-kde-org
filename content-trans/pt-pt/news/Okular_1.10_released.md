---
date: 2020-04-23
title: O Okular 1.10 foi lançado
---
A versão 1.10 do Okular foi lançada. Esta versão introduz o deslocamento cinético, melhoras na gestão de páginas, melhoria na interface para dispositivos móveis e diversas correcções e funcionalidades pequenas em todo o lado. Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=20.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=20.04.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
