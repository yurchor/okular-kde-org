---
date: 2016-08-18
title: O Okular 0.26 foi lançado
---
A versão 0.26 do Okular foi lançada em conjunto com a versão 16.08 das Aplicações do KDE. Esta versão introduz pequenas correcções de erros e funcionalidades, podendo verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
