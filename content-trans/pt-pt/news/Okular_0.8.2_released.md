---
date: 2009-04-02
title: O Okular 0.8.2 foi lançado
---
A segunda versão de manutenção da série KDE 4.2 inclui o Okular 0.8.2. Inclui um melhor suporte (e supostamente funcional) para a pesquisa inversa em DVI e com o 'pdfsync', assim como algumas correcções e pequenas melhorias no modo de apresentação. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
