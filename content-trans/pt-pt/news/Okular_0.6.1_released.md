---
date: 2008-02-05
title: O Okular 0.6.1 foi lançado
---
A primeira versão de manutenção da série KDE 4.0 inclui o Okular 0.6.1. Este inclui algumas correcções de erros, incluindo a correcção da transferência sucessiva dos ficheiros ao gravar, algumas melhorias de usabilidade, etc. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
