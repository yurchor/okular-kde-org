---
date: 2013-02-06
title: O Okular 0.16 foi lançado
---
A versão 0.16 do Okular foi lançada em conjunto com a versão 4.10 das Aplicações do KDE. Esta versão introduz novas funcionalidades, como a ampliação para níveis maiores nos documentos PDF, um visualizador baseado no Active para as suas tabletes, mais suporte para filmes em PDF, seguimento da área de visualização pela selecção e melhorias na edição de anotações. O Okular 0.16 é uma actualização recomendada para todos os que usam o Okular.
