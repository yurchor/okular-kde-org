---
date: 2014-12-17
title: O Okular 0.21 foi lançado
---
A versão 0.21 do Okular foi lançada em conjunto com a versão 14.12 das Aplicações do KDE. Esta versão introduz novas funcionalidades, como o suporte para a pesquisa inversa em latex-synctex para os ficheiros DVI, bem como algumas pequenas correcções de erros. É uma actualização recomendada para todos os que usam o Okular.
