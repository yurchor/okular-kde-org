---
date: 2013-12-12
title: Megjelent az Okular 0.18
---
Az Okular 0.18-es kiadása a KDE Alkalmazások 4.12-vel együtt jelent meg. Ez a kiadás új szolgáltatásokat mutat be, mint például hang- és videotámogatás EPub fájlokban, és a meglévő szolgáltatások fejlesztése. Az Okular 0.18 javasolt frissítés minden Okular felhasználónak.
