---
date: 2008-03-05
title: Megjelent az Okular 0.6.2
---
The second maintenance release of the KDE 4.0 series includes Okular 0.6.2. It includes quite a few bug fixes, including more stability when closing a document, and small fixes for the bookmark system. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
