---
date: 2013-02-06
title: Megjelent az Okular 0.16
---
Az Okular 0.16-os kiadása a KDE Alkalmazások 4.10-zel együtt jelent meg. Ez a kiadás új szolgáltatásokat vezet be mint a magasabb szintű nagyítások képessége a PDF dokumentumokban, egy Active alapú megjelenítő a táblagépekhez, több támogatás a PDF filmekhez, a munkalap követi a kijelölést, és megjegyzés szerkesztés fejlesztések. Az Okular 0.16 javasolt frissítés minden Okular felhasználónak.
