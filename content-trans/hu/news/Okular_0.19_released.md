---
date: 2014-04-16
title: Megjelent az Okular 0.19
---
The 0.19 version of Okular has been released together with KDE Applications 4.13 release. This release introduces new features like tabs support in the interface, use of DPI screen so that page size matches real paper size, improvements to the Undo/Redo framework and other features/refinements. Okular 0.19 is a recommended update for everyone using Okular.
