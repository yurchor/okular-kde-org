---
date: 2010-02-09
title: Megjelent az Okular 0.10
---
Az Okular 0.10-es kiadása a KDE Applications 4.4-gyel eggyütt jelent meg. Ebben a kiadásban kisebb javítások és új funkciók szerepelnek, a frissítés minden Okular felhasználónak ajánlott.
