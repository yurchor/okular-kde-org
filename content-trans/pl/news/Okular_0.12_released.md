---
date: 2011-01-26
title: Okular 0.12 wydany
---
Wersja 0.12 programu Okular została opublikowana wraz z Aplikacjami KDE w wersji 4.6. To wydanie wprowadza drobne poprawki oraz funkcje i jest zalecaną aktualizacją dla wszystkich użytkowników Okular.
