---
date: 2018-12-13
title: Okular 1.6 wydany
---
Wersja 1.6 Okulara została wydana wraz z wydaniem Aplikacji do KDE 18.12. To wydanie wprowadza nowe narzędzie przypisów maszyny do pisania  poza innymi poprawkami i małym nowościami. Pełny dziennik zmian możesz obejrzeć na  a <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.6 wszystkim użytkownikom.
