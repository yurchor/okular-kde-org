---
date: 2009-06-03
title: Okular 0.8.4 wydany
---
Czwarte wydanie podtrzymujące linię KDE 4.2 obejmuje Okular 0.8.4. Zawiera ono poprawki dokumentów tekstowych OpenDocument. Oprócz tego naprawiono kilka awarii oraz błędy w interfejsie. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
