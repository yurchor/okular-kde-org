---
date: 2009-04-02
title: Okular 0.8.2 wydany
---
Drugie wydanie podtrzymujące linię KDE 4.2 obejmuje Okular 0.8.2. Zawiera ono ulepszone wsparcie (mamy nadzieję, że działające) dla DVI, wyszukiwanie odwrotne pdfsync oraz drobne poprawki i ulepszenia w trybie prezentacji. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
