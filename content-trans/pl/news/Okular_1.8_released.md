---
date: 2019-08-15
title: Okular 1.8 wydany
---
Wersja 1.8 Okulara została wydana wraz z wydaniem Aplikacji do KDE 19.08. To wydanie daje możliwość wybrania rodzajów zakończeń linii przypisów poza innymi poprawkami i małym nowościami. Pełny dziennik zmian możesz obejrzeć na  a <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.8 wszystkim użytkownikom.
