---
date: 2009-12-01
title: Okular 0.9.4 wydany
---
Czwarte wydanie podtrzymujące linię KDE 4.3 obejmuje Okular 0.9.4. Zawiera ono kilka poprawek awarii i drobnych błędów w interfejsie. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
