---
date: 2019-12-12
title: Okular 1.9 wydany
---
Wersja 1.9 Okulara została wydana. To wydanie wprowadza obsługę archiwów ComicBook cb7, ulepszoną obsługę JavaScript dla plików PDF poza innymi poprawkami i małym nowościami. Pełny dziennik zmian możesz obejrzeć na  a <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.9 wszystkim użytkownikom.
