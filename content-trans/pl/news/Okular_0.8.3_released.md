---
date: 2009-05-06
title: Okular 0.8.3 wydany
---
Trzecie wydanie podtrzymujące linię KDE 4.2 obejmuje Okular 0.8.3. Nie wprowadza wielu nowości do programu Okular. Jedyna istotna zmiana dotyczy większego bezpieczeństwa wątku podczas generowania obrazów stron dokumentu XPS. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
