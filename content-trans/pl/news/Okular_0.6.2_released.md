---
date: 2008-03-05
title: Okular 0.6.2 wydany
---
Drugie wydanie podtrzymujące linię KDE 4.0 obejmuje Okular 0.6.2. Zawiera sporo poprawek błędów, w tym większą stabilność podczas zamykania dokumentów i drobne poprawki systemu zakładek. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
