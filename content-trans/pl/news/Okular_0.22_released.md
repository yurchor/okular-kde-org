---
date: 2015-04-15
title: Okular 0.22 wydany
---
Wersja 0.22 programu Okular została wydana wraz z wydaniem Aplikacji KDE 15.04. Okular 0.22 jest zalecaną aktualizacją dla wszystkich użytkowników Okulara.
