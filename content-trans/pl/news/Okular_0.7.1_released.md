---
date: 2008-09-03
title: Okular 0.7.1 wydany
---
Pierwsze wydanie podtrzymujące linię KDE 4.1 obejmuje Okular 0.7.1. Zawiera ono ważne poprawki pomiędzy drobnymi ulepszeniami. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
