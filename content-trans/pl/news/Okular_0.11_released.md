---
date: 2010-08-10
title: Okular 0.11 wydany
---
Wersja 0.11 programu Okular została opublikowana wraz z Aplikacjami KDE w wersji 4.5. To wydanie wprowadza drobne poprawki oraz funkcje i jest zalecaną aktualizacją dla wszystkich użytkowników Okular.
