---
date: 2018-08-16
title: Okular 1.5 wydany
---
Wersja 1.5 Okulara została wydana wraz z wydaniem Aplikacji do KDE 18.08. To wydanie wprowadza ulepszenia do formularzy poza innymi poprawkami i małym nowościami. Pełny dziennik zmian możesz obejrzeć na  a <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.5 wszystkim użytkownikom.
