---
date: 2013-08-16
title: Okular 0.17 wydany
---
Wersja 0.17 Okulara została wydana wraz z wydaniem Aplikacji do KDE 4.11. Wydanie to wprowadza funkcje takie jak obsługa cofnij/ponów dla formularzy i przypisów oraz konfigurowalne narzędzie recenzji. Okular 0.17 jest zalecanym uaktualnieniem dla wszystkich go używających.
