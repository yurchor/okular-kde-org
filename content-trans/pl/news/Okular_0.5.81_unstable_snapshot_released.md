---
date: 2006-11-02
title: Okular 0.5.81 (wersja niestabilna) wydany
---
The Okular team is proud to announce the release of a snapshot of Okular that compiles against the <a href="http://dot.kde.org/1162475911/">Second KDE 4 Developers Snapshot</a>. This snapshot is not completely functional, yet, as we have lots of things to polish and finish, but you are free to test it and provide as much feedback as you want. You can find the snapshot package at <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Have a look at the <a href="download.php">download</a> page to be sure you have all the necessary libraries.
