---
date: 2014-12-17
title: Okular 0.21 wydany
---
Wersja 0.21 Okulara została wydana wraz z wydaniem Aplikacji do KDE 14.12. Wydanie to wprowadza funkcje takie jak wyszukiwanie wsteczne latex-synctex w dvi, a także naprawiono w nim małe błędy. Okular 0.21 jest zalecanym uaktualnieniem dla wszystkich go używających.
