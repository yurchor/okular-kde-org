---
menu:
  main:
    name: Compiler le
    parent: about
    weight: 2
title: Compilation d'Okular depuis les sources sous Linux
---
<span style="background-color:#e8f4fa">Si vous recherchez des paquets pré compilés, veuillez consulter la [page de téléchargement](/download/). Vous pourrez vérifier l'état du paquet [ici](https://repology.org/project/okular/versions)</span>.

Si vous voulez compiler Okular, vous avez besoin d'avoir un environnement de compilation correctement paramétré, en général, cela devrait être fourni dans votre distribution. Dans le cas où vous voulez compiler la version de développement de Okular, veuillez vous reporter à la page « Compiler depuis les sources » sur le wiki de la communauté de KDE.

Vous pouvez télécharger et compiler Okular comme suit :

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Si vous installez Okular dans un emplacement différent que votre emplacement d'installation du système, il est possible que vous deviez exécuter « source build/prefix.sh ; okular` », afin que l'instance correcte d'Okular et de ses bibliothèques soient prises en compte.

## Paquets optionnels

Il y a quelques paquets optionnels que vous pourriez installer de façon à profiter de quelques fonctionnalités supplémentaires dans Okular. Certains pourraient être déjà empaquetés pour votre distribution et d'autres non. Si vous voulez éviter tout problème, n'utilisez que les paquets pris en charge par votre distribution.

* Poppler (Moteur PDF backend) : pour compiler le moteur « PDF », vous avez besoin de la [bibliothèque « Poppler »](http://poppler.freedesktop.org), avec une version supérieure à la version 0.24.
* Libspectre : afin de compiler et d'utiliser le moteur PostScipt (PS), vous avez besoin du paquet «  libspectre » en version supérieure ou égale à 0.2. Si votre distribution ne le propose pas sous forme de paquet our si la version de paquet n'est pas suffisante, vous pouvez le télécharger [ici](http://libspectre.freedesktop.org).
* DjVuLibre : pour compiler le moteur « DjVu », vous avez besoin de la bibliothèque « DjVuLibre » avec une version supérieure à 3.5.17. Comme pour la bibliothèque « Libspectre », vous pouvez l'obtenir à partir de votre distribution ou [ici](http://djvulibre.djvuzone.org).
* libTIFF : celle-ci est nécessaire pour la prise en charge de TIFF / fax. Actuellement, il n'y a pas de version minimale requise. Ainsi, toute version assez récente disponible de votre distribution devrait fonctionner. En cas de problème, n'hésitez pas à prendre contact avec les développeurs d'Okular.
* libCHM : ce dernier est nécessaire pour compiler le moteur « CHM ». Comme avec libTIFF, il n'y a aucune exigence de version minimale. 
* Libepub : si vous avez besoin de la prise en charge de « EPub », vous pouvez installer cette bibliothèque à partir de votre distribution ou à partir de [sourceforge](http://sourceforge.net/projects/ebook-tools).
