---
intro: Okular est disponible comme un paquet pré compilé sur une large variété de
  plate-formes. Vous pouvez vérifier l'état du paquet pour votre distribution Linux
  sur la droite ou poursuivre la lecture des informations sur d'autres systèmes d'exploitation.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular est déjà disponible sur la plupart des distributions Linux. Vous pouvez
    l'installer à partir du [Centre des applications de KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logo de Flatpak
  name: Flatpak
  text: Vous pouvez installer la dernière version par [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    à partir de Flathub. Des Flatpaks expérimentaux avec les compilations de développement
    peuvent être [installées à partir du dépôt Flatpak de KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logo de Ark
  name: Publier les sources
  text: Okular est publié de façon fréquente comme partie des applications de KDE
    Gear. Si vous voulez le compiler à partir des sources, vous pouvez vérifier la
    [section « Compiler le » ](/build-it).
- image: /images/windows.svg
  image_alt: Logo de Windows
  name: Windows
  text: Veuillez regarder [l'initiative KDE sous Windows](https://community.kde.org/Windows)
    pour des informations sur comment installer des logiciels de KDE sous Windows.
    La version stable est disponible sous la [boutique Microsoft](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Il y a aussi des [compilations de versions expérimentales](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    pour lesquelles des rapports de tests et de bogues seront les bienvenus.
sassFiles:
- /sass/download.scss
title: Téléchargement
---
