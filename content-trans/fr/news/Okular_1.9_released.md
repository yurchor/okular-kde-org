---
date: 2019-12-12
title: Publication de la version 1.9 d'Okular
---
La version 1.9 d'Okular a été livrée. Cette version intègre la prise en charge de « cb7 » pour les archives de Comic Books, la prise en charge améliorée de «  JavaScript» pour les fichiers « PDF », parmi de nombreuses autres corrections et de petites fonctionnalités. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. La version 1.9 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
