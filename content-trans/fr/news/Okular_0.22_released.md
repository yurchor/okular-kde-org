---
date: 2015-04-15
title: Publication de la version 0.22 d'Okular
---
La version 0.22 d'Okular a été livrée avec les applications fournies avec la version 15.04 des applications KDE. Okular 0.22 est une mise à jour recommandée pour tout utilisateur d'Okular.
