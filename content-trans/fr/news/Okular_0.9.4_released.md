---
date: 2009-12-01
title: Publication de la version 0.9.4 d'Okular
---
La version 0.9.4 d'Okular a été livrée avec les applications fournies avec la quatrième version de maintenance de la famille de KDE 4.3. Elle corrige des plantages et quelques erreurs mineures dans l'interface. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
