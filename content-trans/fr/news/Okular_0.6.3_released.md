---
date: 2008-04-02
title: Publication de la version 0.6.3 d'Okular
---
La version 0.6.3 d'Okular a été livrée avec la troisième version de maintenance de KDE 4.0. Elle apporte quelques corrections d'anomalies, comme par exemple, une meilleure façon d'obtenir la position d'un texte dans un document PDF ainsi qu'un ensemble de corrections pour le système d'annotations et concernant la table des matières. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
