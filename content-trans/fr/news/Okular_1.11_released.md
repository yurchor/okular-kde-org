---
date: 2020-08-13
title: Publication de la version 1.11 d'Okular
---
La version 1.11 d'Okular a été livrée. Cette version intègre une nouvelle interface utilisateur pour les annotations, de nombreuses corrections mineures et enfin certaines fonctionnalités. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. La version 1.11 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
