---
date: 2013-08-16
title: Publication de la version 0.17 d'Okular
---
La version 0.17 d'Okular a été livrée avec les applications fournies avec la version 4.11 de KDE. Cette version intègre des nouvelles fonctionnalités comme la prise en charge de « Annuler » / « Refaire » pour les formulaires et les annotations et que des outils configurables d'analyse. La version 0.17 de Okular est recommandée pour toute personne utilisant Okular. 
