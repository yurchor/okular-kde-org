---
date: 2013-12-12
title: Publication de la version 0.18 d'Okular
---
La version 0.18 d'Okular a été livrée avec les applications fournies avec la version 4.12 de KDE. Cette version intègre des nouvelles fonctionnalités comme la prise en charge de l'audio et de la vidéo dans les fichiers EPub ainsi que l'amélioration de fonctionnalités existantes comme la recherche et l'impression. La version 0.18 d'Okular est recommandée pour toute personne utilisant Okular.
