---
date: 2013-02-06
title: Publication de la version 0.16 d'Okular
---
La version 0.16 d'Okular a été livrée avec les applications fournies avec la version 4.10 de KDE. Cette version intègre de nouvelles fonctionnalités comme la capacité de faire un zoom plus important dans des documents PDF, un afficheur réalisé à partir de « Active » pour vos tablettes, plus de prise en charge des vidéos PDF, sélection de suivi de fenêtre et des améliorations pour les modifications d'annotations. Okular 0.16 est une mise à jour recommandée pour quiconque utilisant Okular.
