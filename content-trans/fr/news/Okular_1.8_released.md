---
date: 2019-08-15
title: Publication de la version 1.8 d'Okular
---
La version 1.8 d'Okular a été livrée avec la version 19.08 des applications de KDE. Cette version intègre la fonctionnalité d'ajout d'annotation en fin de ligne, ainsi que de nombreuses corrections et fonctionnalités mineures. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. La version 1.8 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
