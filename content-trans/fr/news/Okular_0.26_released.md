---
date: 2016-08-18
title: Publication de la version 0.26 d'Okular
---
La version 0.26 d'Okular a été livrée avec les applications fournies avec la version 16.08 de KDE. Cette version intègre des changements mineurs. Vous pouvez lire la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. La version 0.26 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
