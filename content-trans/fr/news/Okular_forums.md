---
date: 2012-10-10
title: Forums d'Okular
---
Un sous-forum existe maintenant parmi les forums de la communauté de KDE. Vous pouvez le trouver à l'adresse <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
