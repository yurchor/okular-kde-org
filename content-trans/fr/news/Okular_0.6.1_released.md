---
date: 2008-02-05
title: Publication de la version 0.6.1 d'Okular
---
La version 0.6.1 d'Okular a été livrée avec la première version de maintenance de KDE 4.0. Elle apporte quelques corrections d'anomalies, comme une meilleure gestion de l'enregistrement limitant le re-téléchargement des fichiers, des améliorations pour faciliter l'utilisation, etc. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
