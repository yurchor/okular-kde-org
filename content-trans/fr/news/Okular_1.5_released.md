---
date: 2018-08-16
title: Publication de la version 1.5 d'Okular
---
La version 1.5 d'Okular a été livrée avec la version 18.08 des applications KDE. Cette version intègre des améliorations des formes ainsi que des corrections et fonctionnalités mineures. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. La version 1.5 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
