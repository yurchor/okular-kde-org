---
date: 2008-10-03
title: Publication de la version 0.7.2 d'Okular
---
La version 0.7.2 d'Okular a été livrée avec la deuxième version de maintenance de KDE 4.1. Elle corrige quelques problèmes mineurs dans dans les moteurs « TIFF » et « ComicBook » ainsi que des changements à « Ajuster la largeur » comme niveau de zoom par défaut. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
