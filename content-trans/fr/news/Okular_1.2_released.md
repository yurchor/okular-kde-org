---
date: 2017-08-17
title: Publication de la version 1.2 d'Okular
---
La version 1.2 d'Okular a été livrée avec la version 17.08 des applications KDE. Cette version intègre des changements mineurs et des améliorations. Vous pouvez lire la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. La version 1.2 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
