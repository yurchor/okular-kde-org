---
date: 2008-03-05
title: Publication de la version 0.6.2 d'Okular
---
La version 0.6.2 d'Okular a été livrée avec la seconde version de maintenance de KDE 4.0. Elle apporte quelques corrections d'anomalies, comme une plus grande stabilité lors de la fermeture d'un document et des corrections mineures concernant la gestion des signets. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
