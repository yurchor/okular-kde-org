---
date: 2015-12-16
title: Publication de la version 0.24 d'Okular
---
La version 0.24 d'Okular a été livrée avec les applications fournies avec la version 15.12 de KDE. Cette version intègre la corrections de problèmes mineurs et de nouvelles fonctionnalités. Vous pouvez lire la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. La version 0.24 de Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
