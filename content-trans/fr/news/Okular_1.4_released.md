---
date: 2018-04-19
title: Publication de la version 1.4 d'Okular
---
La version 1.14 d'Okular a été livrée avec les applications fournies avec la version 18.04 de KDE. Cette version intègre une prise en charge améliorée de JavaScript au sein des fichiers PDF et l'ajout de la prise en charge de l'annulation du rendu PDF. Si vous ouvrez un fichier PDF compliqué et changez de zoom pendant le rendu, celui-ci sera immédiatement annulé au lieu d'aller jusqu'à son terme. Vous pouvez consulter la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. La version 1.4 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular.
