---
date: 2017-04-20
title: Publication de la version 1.1 d'Okular
---
La version 1.1 d'Okular a été livrée avec les applications fournies avec la version 17.04 de KDE. Cette version intègre une fonctionnalité de redimensionnement des annotations, la gestion de calcul automatique des champs de formulaires, des améliorations dans l'utilisation en mode tactile et bien plus ! Vous pouvez lire la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. La version 1.1 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
