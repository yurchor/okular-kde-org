---
date: 2015-08-19
title: Publication de la version 0.23 d'Okular
---
La version 0.23 d'Okular a été livrée avec la version 15.08 des applications KDE. Cette version intègre des nouvelles fonctionnalités comme le fondu enchaîné en mode présentation et des corrections concernant les annotations et la lecture de vidéos. Okular 0.23 est une mise à jour recommandée pour tout utilisateur d'Okular.
