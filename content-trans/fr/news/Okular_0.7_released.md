---
date: 2008-07-29
title: Publication de la version 0.7 d'Okular
---
L'équipe d'Okular est fière de vous annoncer la nouvelle version de Okular, publiée comme faisant partie de <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Certaines de ces nouvelles fonctionnalités et améliorations sont (dans un ordre aléatoire) :

* Capacité d'enregistrer un document PD avec les modifications des champs de formulaire.
* Mode Présentation : prise en charge de plus d'un écran et la possibilité de désactiver les transitions
* Le niveau de zoom est maintenant enregistré par document.
* Prise en charge améliorée de la synthèse vocale qui peut être utilisée pour lire la totalité d'un document, d'un page spécifique ou d'une sélection de texte.
* Direction arrière pour la recherche de texte
* Améliorations de la prise en charge des formulaires
* Prise en charge préliminaire (vraiment basique et plus probablement incomplète) pour JavaScript dans les documents « PDF ».
* Prise en charge des annotations des fichiers en pièce jointe
* Capacité de supprimer la bordure blanche des pages lors de leurs affichages.
* Nouveau moteur pour les documents EPub
* Moteur « OpenDocument Text » : prise en charge des documents, les listes et tables chiffrés.
* Moteur XPS : divers améliorations de chargement et de rendu, devrait le rendre maintenant plus utilisable.
* Moteur PS : diverses améliorations, principalement apportées par la version la plus récente de la bibliothèque « libspectre ».
