---
date: 2007-04-04
title: Okular dans « kdegraphics »
---
L'équipe d'Okular est fière de vous annoncer que Okular fait maintenant partie du module « kdegraphics ». La prochaine version d'Okular sera livrée avec KDE 4.0.
