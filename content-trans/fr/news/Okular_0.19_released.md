---
date: 2014-04-16
title: Publication de la version 0.19 d'Okular
---
La version 0.19 d'Okular a été livrée avec les applications fournies avec la version 4.13 de KDE. Cette version intègre des nouvelles fonctionnalités comme la prise en charge des onglets dans l'interface, l'utilisation de la valeur de DPI de l'écran pour que la taille des pages soit celle du papier, des améliorations des fonctions annuler/refaire et d'autres fonctionnalités/améliorations. La version 0.19 d'Okular est recommandée pour toute personne utilisant Okular.
