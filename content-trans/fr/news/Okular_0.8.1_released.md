---
date: 2009-03-04
title: Publication de la version 0.8.1 d'Okular
---
La version 0.8.1 d'Okular a été livrée avec la première version de maintenance de KDE 4.2. Elle ne corrige que quelques plantages dans les moteurs « CHM » et « DjVu » ainsi que des erreurs mineures dans l'interface utilisateur. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
