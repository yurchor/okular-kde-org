---
menu:
  main:
    parent: about
    weight: 4
title: Contacte
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, la mascota de KDE" style="height: 200px;"/>

Podeu contactar amb l'equip de l'Okular de moltes maneres:

* Llista de correu: Per a coordinar el desenvolupament de l'Okular, s'utilitza la [llista de correu okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) a kde.org. Podeu usar-la per a comentar el desenvolupament del nucli de l'aplicació, però també són benvinguts els comentaris quant als dorsals existents o nous.

* IRC: Per al xat general usem l'IRC [#okular](irc://irc.kde.org/#okular) i el [#kde-devel](irc://irc.kde.org/#kde-devel) a la [xarxa Freenode](http://www.freenode.net/). Podreu trobar alguns dels desenvolupadors de l'Okular que rondin per allí.

* Matrix: El xat mencionat abans també es pot accedir des de la xarxa Matrix mitjançant [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Fòrum: Si preferiu usar un fòrum, podeu usar el [fòrum de l'Okular](http://forum.kde.org/viewforum.php?f=251) dins dels [Fòrums de la Comunitat KDE](http://forum.kde.org/).

* Errors i peticions: Els errors i els desitjos s'han de reportar al [seguidor d'errors de KDE](https://bugs.kde.org). Si voleu ajudar, podreu trobar un llista dels errors principals [ací](https://community.kde.org/Okular).
