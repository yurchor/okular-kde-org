---
faq:
- answer: Els paquets de l'Okular a l'Ubuntu (i també el Kubuntu) estan compilats
    sense suport per aquests dos formats. El motiu s'explica en [aquest](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    informe del Launchpad.
  question: En usar l'Ubuntu, no puc llegir documents CHM ni EPub, encara que estan
    instal·lats «okular-extra-backends» i «libchm1». Perquè?
- answer: Perquè no hi ha cap servei de veu en aquest sistema. Instal·leu la biblioteca
    Speech de les Qt i s'haurien d'activar
  question: Perquè hi ha opcions de veu del menú d'eines en gris?
- answer: Instal·leu el paquet «poppler-data»
  question: Hi ha caràcters que no es renderitzen i en activar la depuració hi ha
    línies que mencionen «Missing language pack for xxx»
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Preguntes més freqüents
---
