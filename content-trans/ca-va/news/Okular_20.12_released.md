---
date: 2020-12-10
title: Publicat l'Okular 20.12
---
S'ha publicat la versió 20.12 de l'Okular. Aquest llançament presenta diverses correccions i característiques secundàries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. L'Okular 20.12 és una actualització recomanada per a tothom que usi l'Okular.
