---
date: 2009-05-06
title: Publicat l'Okular 0.8.3
---
La tercera publicació de manteniment de les sèries 4.2 del KDE conté l'Okular 0.8.3. No proporciona moltes novetats per a l'Okular, l'únic canvi important és aportar més seguretat en generar imatges de pàgines als documents XPS. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
