---
date: 2020-08-13
title: Publicat l'Okular 1.11
---
S'ha publicat la versió 1.11 de l'Okular. Aquesta publicació presenta una interfície nova d'usuari de les anotacions i diverses correccions i característiques secundàries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. L'Okular 1.11 és una actualització recomanada per a tothom que usi l'Okular.
