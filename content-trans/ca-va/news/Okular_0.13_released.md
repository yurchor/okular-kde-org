---
date: 2011-07-27
title: Publicat l'Okular 0.13
---
S'ha publicat la versió 0.13 de l'Okular conjuntament amb la publicació 4.7 de les Aplicacions del KDE. Aquesta publicació presenta petites esmenes d'errors i característiques i és una actualització recomanada per a tothom que usi l'Okular.
