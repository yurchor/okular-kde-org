---
date: 2012-10-10
title: Fòrums de l'Okular
---
Ara tenim un subfòrum dins els fòrums de la Comunitat KDE. El trobareu a <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
