---
date: 2009-06-03
title: Publicat l'Okular 0.8.4
---
La quarta publicació de manteniment de les sèries 4.2 del KDE conté l'Okular 0.8.4. Inclou diverses esmenes als documents de text en OpenDocument, un parell d'esmenes de fallades, així com esmenes d'errors a la interfície. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
