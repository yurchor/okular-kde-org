---
intro: Okular unterstützt eine große Zahl von Dokumentformaten und Anwendungsfällen.
  Auf dieser Seite finden Sie immer den Status der aktuellen stabilen Version von
  Okular, zurzeit Okular 20.12.
layout: formats
menu:
  main:
    name: Dokumentformat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status der Unterstützung für Dokumentformate
---
