---
date: 2015-08-19
title: Okular 0.23 veröffentlicht
---
Die Version 0.23 von Okular wurde zusammen mit den KDE-Anwendungen 15.08 veröffentlicht. In dieser Veröffentlichung gibt es Unterstützung für Überblendungen im Präsentationsmodus und auch einige Fehlerkorrekturen für Anmerkungen und die Wiedergabe von Videos. Okular 0.23 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
