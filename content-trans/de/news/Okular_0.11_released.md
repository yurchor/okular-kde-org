---
date: 2010-08-10
title: Okular 0.11 veröffentlicht
---
Die Version 0.11 von Okular wurde zusammen mit den KDE-Programmen 4.5 veröffentlicht. Diese Veröffentlichung enthält kleine Korrekturen und Funktionen und ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
