---
date: 2015-04-15
title: Okular 0.22 veröffentlicht
---
Die Version 0.22 von Okular wurde zusammen mit den KDE-Anwendungen 15.04 veröffentlicht. Diese Veröffentlichung ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
