---
date: 2008-11-04
title: Okular 0.7.3 veröffentlicht
---
Die dritte Wartungsversion der Reihe KDE 4.1 enthält Okular 0.7.3. Diese Version bringt kleinere Korrekturen in der Benutzeroberfläche und der Textsuche. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">hier</a>.
