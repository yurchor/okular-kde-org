---
date: 2008-03-05
title: Okular 0.6.2 veröffentlicht
---
Die zweite Wartungsversion der Reihe KDE 4.0 enthält Okular 0.6.2. Diese Version bringt eine ganze Reihe Korrekturen, wie eine verbesserte Stabilität beim Schließen eines Dokuments und kleinere Korrekturen an den Lesezeichen.Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">hier</a>.
