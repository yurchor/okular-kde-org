---
date: 2010-02-09
title: Okular 0.10 veröffentlicht
---
Die Version 0.10 von Okular wurde zusammen mit KDE SC 4.4 veröffentlicht. Neben allgemeinen Verbesserungen der Stabilität, enthält die Veröffentlichung neue bzw. verbesserte Unterstützung für Vorwärts- und Rückwärtssuche zum Verknüpfen von Latex-Quellcodezeilen mit den entsprechenden Stellen in den DVI- und PDF-Dateien.
