---
date: 2017-08-17
title: Okular 1.2 veröffentlicht
---
Die Version 1.2 von Okular wurde zusammen mit den KDE-Anwendungen 17.08 veröffentlicht. Diese Veröffentlichung enthält kleinere Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
