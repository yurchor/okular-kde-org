---
date: 2016-12-15
title: Okular 1.0 veröffentlicht
---
Die Version 1.0 von Okular wurde heute zusammen mit den KDE-Anwendungen 16.12 veröffentlicht. Diese Veröffentlichung basiert jetzt auf KDE Frameworks 5. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
