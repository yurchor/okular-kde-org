---
date: 2006-08-27
title: Entwicklerschnappschuss Okular 0.5 veröffentlicht
---
Das Okular-Team freut sich, einen Entwicklerschnappschuss von Okular ankündigen zu können, der mit dem <a href="http://dot.kde.org/1155935483/">KDE-4-Schnappschuss „Krash“</a> kompiliert werden kann. Dieser Schnappschuss ist noch nicht voll funktionstüchtig, da noch viele Dinge fertiggestellt und poliert werden müssen. Sie sind aber eingeladen, Okular zu testen, und sich mit Rückmeldungen an die Entwickler zu wenden. Das Paket finden Sie <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">hier</a>. Beachten Sie auch die <a href="download.php">Download</a>-Seite, um sicher zu gehen, dass Sie alle benötigten Bibliotheken installiert haben.
