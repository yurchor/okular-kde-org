---
date: 2014-04-16
title: Okular 0.19 veröffentlicht
---
Die Version 0.19 von Okular wurde zusammen mit den KDE-Programmen 4.13 veröffentlicht. Diese Veröffentlichung enthält neue Funktionen wie Unterfenster in der grafischen Oberfläche und benutzt die DPI (Dot per inch - Punktdichte) des Bildschirms, so dass die Seitengröße der Anzeige dem tatsächlichen Papierformat entspricht. Die Funktion Zurücknehmen/Wiederherstellen und weitere Funktionen wurden verbessert und überarbeitet. Okular 0.19 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
