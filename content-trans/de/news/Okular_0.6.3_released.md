---
date: 2008-04-02
title: Okular 0.6.3 veröffentlicht
---
Die dritte Wartungsversion der Reihe KDE 4.0 enthält Okular 0.6.3. Diese Version bringt einige Korrekturen, z. B. Verbesserungen in der Bestimmung der Textposition in PDF-Dokumenten sowie Korrekturen am Anmerkungen-System und am Inhaltsverzeichnis.Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">hier</a>.
