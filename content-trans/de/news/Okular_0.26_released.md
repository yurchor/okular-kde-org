---
date: 2016-08-18
title: Okular 0.26 veröffentlicht
---
Die Version 0.26 von Okular wurde zusammen mit den KDE-Anwendungen 16.08 veröffentlicht. Diese Veröffentlichung enthält kleinere Änderungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0</a>. Okular 0.26 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
