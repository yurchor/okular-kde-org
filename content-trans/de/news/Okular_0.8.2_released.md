---
date: 2009-04-02
title: Okular 0.8.2 veröffentlicht
---
Die zweite Wartungsversion der Reihe KDE 4.2 enthält Okular 0.8.2. Diese Version bringt bessere Unterstützung für Rückwärtssuche in DVI- und pdfsync-Dokumenten und Korrekturen und einige Verbesserungen im Präsentationsmodus. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">hier</a>.
