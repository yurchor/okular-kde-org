---
date: 2009-03-04
title: Okular 0.8.1 veröffentlicht
---
Die erste Wartungsversion der Reihe KDE 4.2 enthält Okular 0.8.1. Diese Version bringt Korrekturen, die Abstürze in den Backends für CHM und DjVu verhindern und behebt ein paar kleine Fehler in der Benutzeroberfläche. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">hier</a>.
