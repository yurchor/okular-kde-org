---
date: 2011-07-27
title: Okular 0.13 veröffentlicht
---
Die Version 0.13 von Okular wurde zusammen mit den KDE-Programmen 4.7 veröffentlicht. Diese Veröffentlichung enthält kleine Korrekturen und Funktionen und ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
