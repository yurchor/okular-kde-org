---
date: 2008-10-03
title: Okular 0.7.2 veröffentlicht
---
Die zweite Wartungsversion der Reihe KDE 4.1 enthält Okular 0.7.2. Diese Version bringt ein paar kleinere Korrekturen in den Backends für TIFF und Comicbook und ändert die Voreinstellung der Betrachtungsgröße auf „Seitenbreite“. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">hier</a>.
