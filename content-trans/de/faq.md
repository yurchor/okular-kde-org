---
faq:
- answer: Die Okular-Pakete von Ubuntu (und daher auch von Kubuntu) sind ohne Unterstützung
    für diese zwei Formate kompiliert. Den Grund dafür wird in  [diesem](https://
    bugs.launchpad.net/kdegraphics/+bug/277007) Launchpad-Fehlerbericht erklärt.
  question: Warum kann ich mit Ubuntu keine CHM- und EPUB-Dokumente lesen, obwohl
    ich „okular-extra-backends“ und „libchm1“ installiert habe?
- answer: Das liegt daran, dass kein Sprachausgabesystem installiert ist. Installieren
    Sie die Bibliothek Qt Speech, die Menüpunkte sollten dann verfügbar sein.
  question: Warum sind die Sprachausgabe-Optionen im Menü „Extras“ ausgegraut?
- answer: Installieren Sie das Paket poppler-data
  question: Einige Zeichen werden nicht angezeigt und mit aktiviertem Debug gibt es
    Zeilen wie „Missing language pack for xxx“
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Häufig gestellte Fragen
---
