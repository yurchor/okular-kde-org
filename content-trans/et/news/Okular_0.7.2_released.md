---
date: 2008-10-03
title: Ilmus Okular 0.7.2
---
KDE 4.1 seeria teine hooldusväljalase sisaldab Okulari 0.7.2. See sisaldab mõningaid väiksemaid parandusi TIFF-i ja Comicbooki taustaprogrammis ning "Mahuta laiusele" muutmist vaikimis suurendustasemeks. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
