---
date: 2013-12-12
title: Ilmus Okular 0.18
---
Okulari versioon 0.8 ilmus koos KDE rakenduste väljalaskega 4.12. See väljalase sisaldab uusi võimalusi, näiteks heli ja video toetus EPubi failides, ja parandusi senistes võimalustes, näiteks otsimises ja trükkimises. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.18 peale.
