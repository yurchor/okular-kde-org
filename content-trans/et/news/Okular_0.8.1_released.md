---
date: 2009-03-04
title: Ilmus Okular 0.8.1
---
KDE 4.2 seeria neljas hooldusväljalase sisaldab Okulari 0.8.1. See sisaldab mõningate krahhide parandamist CHM ja DjVu  taustaprogrammides ja väiksemaid veaparandusi liideses. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
