---
date: 2009-12-01
title: Ilmus Okular 0.9.4
---
KDE 4.3 seeria neljas hooldusväljalase sisaldab Okulari 0.9.4. See sisaldab mõningate krahhide parandamist ja väiksemaid veaparandusi liideses. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
