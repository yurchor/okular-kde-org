---
date: 2013-08-16
title: Ilmus Okular 0.17
---
Okulari versioon 0.17 ilmus koos KDE rakenduste väljalaskega 4.11 See väljalase sisaldab uusi võimalusi, näiteks taasivõtmise/uuestitegemise toetus vormides ja annotatsioonides ning seadistatavad annoteerimise tööriistad. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.17 peale.
