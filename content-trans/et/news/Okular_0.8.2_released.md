---
date: 2009-04-02
title: Ilmus Okular 0.8.2
---
KDE 4.2 seeria teine hooldusväljalase sisaldab Okulari 0.8.2. See sisaldab paremat (ja loodetavasti toimivat) DVI ja pdfsynci pöördotsingu toetust ning väiksemaid parandusi esitlusrežiimis. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
