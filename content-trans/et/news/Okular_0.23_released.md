---
date: 2015-08-19
title: Ilmus Okular 0.23
---
Okulari versioon 0.23 ilmus koos KDE rakenduste väljalaskega 15.08. See väljalase sisaldab hääbuva ülemineku toetust esitlusrežiimis ning mõningaid parandusi annotatsioonide ja video esitamise toetusel. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.23 peale.
