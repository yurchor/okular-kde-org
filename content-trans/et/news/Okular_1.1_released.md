---
date: 2017-04-20
title: Ilmus Okular 1.1
---
Okulari versioon 1.1 ilmus koos KDE rakenduste väljalaskega 17.04. See väljalase sisaldab annotatsioonide suuruse muutmise võimalust, toetab vormide sisu automaatset väljaarvutamist, parandab puuteekraani toetust ja veel palju muud! Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.1 peale.
