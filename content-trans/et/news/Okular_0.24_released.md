---
date: 2015-12-16
title: Ilmus Okular 0.24
---
Okulari versioon 0.24 ilmus koos KDE rakenduste väljalaskega 15.12. See väljalase sisaldab väiksemaid parandusi ja täiustusi. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.24 peale.
