---
date: 2008-09-03
title: Ilmus Okular 0.7.1
---
KDE 4.1 seeria esimene hooldusväljalase sisaldab Okulari 0.7.1. See sisaldab mõningate krahhide parandamist ja   väiksemaid veaparandusi. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
