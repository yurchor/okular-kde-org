---
intro: Okular доступний у форматі вже зібраного пакунка на широкому діапазоні платформ.
  Ви можете ознайомитися зі станом пакування для вашого дистрибутива Linux праворуч
  або почитати нижче про те, як реалізовано пакування у інших операційних системах.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Тукс
  name: Linux
  text: Okular вже доступний у більшості дистрибутивів Linux. Ви можете встановити
    його з [Центру програмного забезпечення KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Логотип Flatpak
  name: Flatpak
  text: Ви можете встановити найсвіжіший [Flatpak Okular](https://flathub.org/apps/details/org.kde.okular)
    з Flathub. Експериментальні Flatpak-и для щоденних збірок Okular можна [встановити
    зі сховища Flatpak KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Логотип Ark
  name: Початкові коди випуску
  text: Нові версії Okular регулярно випускаються як частина комплектів програм KDE
    (KDE Gear). Якщо ви хочете зібрати програму з початкового коду, ви можете ознайомитися
    із вмістом [розділу, присвяченого збиранню](/build-it).
- image: /images/windows.svg
  image_alt: Логотип Windows
  name: Windows
  text: Зверніться до сторінки [ініціативи KDE у Windows](https://community.kde.org/Windows),
    щоб дізнатися про те, як встановити програми KDE у Windows. Стабільний випуск
    можна знайти у [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Також можна скористатися [експериментальними щоденними збірками](https://binary-factory.kde.org/job/Okular_Nightly_win64/).
    Будемо раді побачити ваші результати тестування та звіти щодо вад.
sassFiles:
- /sass/download.scss
title: Отримання
---
