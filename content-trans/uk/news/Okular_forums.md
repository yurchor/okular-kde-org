---
date: 2012-10-10
title: Форум Okular
---
Тепер у нас є підфорум у комплексі форумів спільноти KDE. Знайти його можна за адресою <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
