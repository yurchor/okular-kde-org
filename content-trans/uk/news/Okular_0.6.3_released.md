---
date: 2008-04-02
title: Випущено Okular 0.6.3
---
До складу третьої модифікації випуску KDE 4.0 включено Okular 0.6.3. У новій версії виправлено декілька вад, зокрема покращено позиціювання тексту у документах PDF, виправлено декілька вад у системі анотування та показі вмісту документів. Повний список виправлених вад можна знайти <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">тут</a>
