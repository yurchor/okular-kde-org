---
date: 2014-12-17
title: Випущено Okular 0.21
---
Разом з набором програм KDE 14.12 випущено версію 0.21 Okular. У цьому випуску ви зможете скористатися новими можливостями, зокрема зворотним пошуком у коді LaTeX з DVI за допомогою synctex, а також виправленнями вад. Рекомендуємо оновитися до цієї версії усім, хто користується Okular.
