---
date: 2017-08-17
title: Випущено Okular 1.2
---
Разом з набором програм KDE 17.08 випущено версію 1.2 Okular. У цьому випуску ви зможете скористатися незначними виправленнями вад та поліпшеннями. Із повним списком змін можна ознайомитися <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>тут</a>. Рекомендуємо оновитися до версії 1.2 усім, хто користується Okular.
