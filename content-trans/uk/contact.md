---
menu:
  main:
    parent: about
    weight: 4
title: Контакти
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

Існує багато способів, у які можна зв’язатися з командою розробників Okular:

* Список листування: для координування розробки okular ми використовуємо [список листування okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) на сервері kde.org. Ви можете скористатися списком листування для спілкування з приводу розробки програми і для надання відгуків щодо поточних або бажаних модулів програми.

* IRC: для спілкування із загальних питань ми використовуємо канали [#okular](irc://irc.kde.org/#okular) і [#kde-devel](irc://irc.kde.org/#kde-devel) у [мережі Freenode](http://www.freenode.net/) IRC. Там ви зможете поспілкуватися із розробниками Okular.

* Matrix: доступ до згаданого вище майданчика спілкування можна також отримати за допомогою мережі Matrix, а саме [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Форум: якщо ви надаєте перевагу спілкуванню на форумах, ви можете скористатися [форумом Okular](http://forum.kde.org/viewforum.php?f=251), частиною великого [комплексу форумів спільноти KDE](http://forum.kde.org/).

* Вади і побажання: про вади і побажання слід повідомляти за допомогою [системи стеження за вадами у KDE](https://bugs.kde.org/). Якщо ви хочете допомогти, список найважливіших вад можна знайти [тут](https://community.kde.org/Okular).
