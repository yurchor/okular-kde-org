---
date: 2008-10-03
title: Okular 0.7.2 已发布
---
The second maintenance release of the KDE 4.1 series includes Okular 0.7.2. It includes some minor fixes in the TIFF and Comicbook backends and the change to "Fit Width" as default zoom level. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
