---
date: 2018-08-16
title: Okular 1.5 已发布
---
The 1.5 version of Okular has been released together with KDE Applications 18.08 release. This release introduces Form improvements among various other fixes and small features. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 is a recommended update for everyone using Okular.
