---
date: 2007-04-04
title: Okular in kdegraphics
---
The Okular team is proud to announce Okular is now part of the kdegraphics module. The next version of Okular will be shipped with KDE 4.0.