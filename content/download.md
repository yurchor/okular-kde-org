---
layout: download
title: Download
menu:
  main:
   parent: about
   weight: 3
intro: Okular is available as a precompiled package in a wide range of platforms. You can check the package status for your Linux distro on the right or keep reading for info on other operating systems
sassFiles:
  - /sass/download.scss
options: 
    - name: Linux
      image: /images/tux.png
      image_alt: Tux
      text: "Okular is already available on most Linux distributions. You can install it from the [KDE Software Center](https://apps.kde.org/okular)."
    - name: Flatpak
      image: /images/flatpak.png
      image_alt: Flatpak logo
      text: "You can install the latest [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular) from Flathub. Experimental Flatpaks with nightly builds of Okular can be [installed from the KDE Flatpak repository](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications )."
    - name: Release Sources
      image: /images/ark.svg
      image_alt: Ark logo
      text: "Okular is released regularly as part of KDE Gear. If you want to build from source, you can check the [Build It section](/build-it)."
    - name: Windows
      image: /images/windows.svg
      image_alt: Windows logo
      text: "Have a look at the [KDE on Windows initiative](https://community.kde.org/Windows) for information on how to install KDE Software on Windows. The stable release is available on the [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8). There are also [experimental nightly builds](https://binary-factory.kde.org/job/Okular_Nightly_win64/), for which testing and bug reports would be welcome."
      
---



