---
layout: formats
title: Document Format Handlers Status
intro: Okular supports a wide variety of document formats and use cases. This page always refers to the stable series of Okular, currently Okular 20.12
menu:
  main:
   parent: about
   weight: 1
   name: Document Format
sassFiles:
  - /sass/table.scss
---
