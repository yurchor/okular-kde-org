# Okular.kde.org Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_okular-kde-org)](https://binary-factory.kde.org/job/Website_okular-kde-org/)

This is the git repository for okular.kde.org, the website for Okular, the universal document reader developed by KDE.

As a (Hu)Go module, it requires both [Hugo](https://gohugo.io/) and Go to work.

## Development
Read [kde-hugo wiki](https://invent.kde.org/websites/aether-sass/-/wikis/Hugo).

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).
